﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace OrangeStripes
{
    public class PlayerMetabolism : MonoBehaviour, IDamageable
    {
        private NetworkView netView;
        private MultiplayerManager mg;
        private bool initialSetup = false;

        //VARIABLES

        private float lerpSpeed = 7;
        private Transform playerUi;

        private bool isAlive = true;
        private bool isBleeding;

        private float currentHealth = 100;
        private float mappedHealth;
        private float maxHealth = 100;

        private Image hpBar;

        private float currentStamina = 100;
        private float mappedStamina;
        private float maxStamina = 100;
        private bool usedStamina;
        private float timeSinceStaminaUsed = 0;
        private float staminaRegenRate = .3f;

        private Image stamBar;

        private float bleedingStrength = 1f;

        private PlayerController controller;
        private RagdollManager ragdolls;
        private CapsuleCollider collider;

        private float pushPower;
        private Vector3 pushOrigin;
        private bool pushed;
        private bool blocked;

        private GameObject deathScreen;

        private float baseDamageResistance = 1;
        private float damageResistance = 1;

        //PROPERTIES

        public bool IsAlive
        {
            get
            {
                return isAlive;
            }
        }

        public bool IsBleeding
        {
            get
            {
                return isBleeding;
            }
        }

        public float CurrentHealth
        {
            get
            {
                return currentHealth;
            }
            set
            {
                currentHealth = Mathf.Clamp(value, 0, maxHealth);
            }
        }

        public float MaxHealth
        {
            get
            {
                return maxHealth;
            }
            set
            {
                maxHealth = value;
            }
        }

        public float CurrentStamina
        {
            get
            {
                return currentStamina;
            }
            set
            {
                currentStamina = Mathf.Clamp(value, 0, maxStamina);
            }
        }

        public float MaxStamina
        {
            get
            {
                return maxStamina;
            }
            set
            {
                maxStamina = value;
            }
        }

        public float PushPower
        {
            get
            {
                return pushPower;
            }
            set
            {
                pushPower = value;
            }
        }

        public bool Pushed
        {
            get
            {
                return pushed;
            }
            set
            {
                pushed = value;
            }
        }

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
            controller = GetComponent<PlayerController>();
            ragdolls = GetComponent<RagdollManager>();
            collider = GetComponent<CapsuleCollider>();
        }

        void InitialSetup()
        {
            if(mg)
            {
                if (mg.MapLoaded && !initialSetup)
                {
                    playerUi = GameObject.Find("PlayerCanvas").transform;
                    playerUi.gameObject.SetActive(true);
                    deathScreen = GameObject.Find("DeathCanvas");
                    hpBar = playerUi.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>();
                    stamBar = playerUi.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>();
                    DisableDeathScreen();
                    initialSetup = true;
                }
            }            
        }

        void Start()
        {
            if(netView.isMine)
            {
                if (GameManager.instance)
                {
                    if (GameManager.instance.MultiplayerManager)
                    {
                        mg = GameManager.instance.MultiplayerManager;
                    }
                }

                InitialSetup();
            }
        }

        void Update()
        {
            if(netView.isMine)
            {
                if (mg)
                {
                    if (!initialSetup && mg.MapLoaded)
                    {
                        InitialSetup();
                    }
                }
                else
                { 
                    if (GameManager.instance)
                    {
                        if (GameManager.instance.MultiplayerManager)
                        {
                            mg = GameManager.instance.MultiplayerManager;
                        }
                    }
                }
            }

            if(blocked)
            {
                controller.Blocked = true;
                blocked = false;
            }

            UpdateCollider();

            currentStamina = Mathf.Clamp(currentStamina, 0, maxStamina);

            if (netView && netView.isMine && initialSetup)
            {
                HandleDisplay();
            }

            if(currentHealth <= 0 && isAlive)
            {
                netView.RPC("Die", RPCMode.AllBuffered);
            }

            if(!isAlive)
            {
                currentHealth = 0;
                ragdolls.RagdollPlayer();
                if (netView.isMine)
                {
                    GetComponent<Hotbar>().DropEverything();
                    GetComponent<PlayerSetup>().DisableControls();
                    EnableDeathScreen();
                }
            }

            if (isAlive)
            {
                if (isBleeding)
                {
                    currentHealth -= bleedingStrength;
                }

                if (pushed)
                {
                    GetComponent<Rigidbody>().AddForceAtPosition(pushOrigin * pushPower, transform.forward);
                    if (netView.isMine)
                    {
                        controller.GotHit = true;

                        if (controller.Blocking)
                        {
                            controller.Interrupted = true;
                        }
                    }
                    pushed = false;
                }

                if(netView.isMine)
                {
                    if(usedStamina)
                    {
                        timeSinceStaminaUsed += Time.deltaTime;
                    }

                    if(timeSinceStaminaUsed >= 2)
                    {
                        usedStamina = false;
                        currentStamina += staminaRegenRate;
                    }
                }
            }
        }

        void HandleDisplay()
        {
            mappedHealth = currentHealth / maxHealth;

            if(mappedHealth != hpBar.fillAmount)
            {
                hpBar.fillAmount = Mathf.Lerp(hpBar.fillAmount, mappedHealth, Time.deltaTime * lerpSpeed);
            }

            mappedStamina = currentStamina / maxStamina;

            if(mappedStamina != stamBar.fillAmount)
            {
                stamBar.fillAmount = Mathf.Lerp(stamBar.fillAmount, mappedStamina, Time.deltaTime * lerpSpeed);
            }
        }

        void UpdateCollider()
        {
            if (isAlive)
            {
                if (collider.direction != 1)
                {
                    collider.center = new Vector3(0, 1.9f, 0);
                    collider.direction = 1;                    
                }
            }
            else
            {
                if (!collider.isTrigger)
                {
                    GetComponent<Rigidbody>().isKinematic = true;
                    collider.isTrigger = true;
                }
            }
        }

        public void Damage(float damage, EDamageType type, string limb)
        {
            switch(type)
            {
                case EDamageType.Blunt:

                    break;

                case EDamageType.Bullet:

                    break;

                case EDamageType.Cut:

                    break;

                case EDamageType.Fall:

                    break;

                case EDamageType.Fire:

                    break;
            }

            CalculateDmg(damage, limb);

            netView.RPC("UpdateMyHealth", RPCMode.AllBuffered, currentHealth);
        }

        public void BlockHit()
        {
            netView.RPC("TellIBlockedHit", RPCMode.All);
        }

        void CalculateDmg(float dmg, string limb)
        {
            float multiplier = 1;

            switch(limb)
            {
                case "Hips":
                    multiplier = 0.95f;
                    break;
                case "Chest":
                    multiplier = 1;
                    break;
                case "Head":
                    multiplier = 1.75f;
                    break;
                case "RightArm":
                    multiplier = 0.85f;
                    break;
                case "LeftArm":
                    multiplier = 0.85f;
                    break;
                case "RightForeArm":
                    multiplier = 0.8f;
                    break;
                case "LeftForeArm":
                    multiplier = 0.8f;
                    break;
                case "RightUpLeg":
                    multiplier = 0.85f;
                    break;
                case "LeftUpLeg":
                    multiplier = 0.85f;
                    break;
                case "RightLeg":
                    multiplier = 0.8f;
                    break;
                case "LeftLeg":
                    multiplier = 0.8f;
                    break;
            }

            currentHealth -= (dmg * multiplier) * damageResistance;
        }

        public void UseStamina(float amount)
        {
            currentStamina -= amount;
            usedStamina = true;
            timeSinceStaminaUsed = 0;
        }

        public void Heal(float amount)
        {
            currentHealth += amount;
            netView.RPC("UpdateMyHealth", RPCMode.AllBuffered, currentHealth);
        }

        public void Bleed(float power)
        {
            bleedingStrength = power;
            isBleeding = true;
        }

        void OnPlayerConnected(NetworkPlayer player)
        {
            netView.RPC("UpdateMyHealth", player, currentHealth);
        }

        public void GetPushed(float power, Vector3 origin)
        {
            netView.RPC("AddForceFromPush", RPCMode.All, power, origin);
        }

        void EnableDeathScreen()
        {
            deathScreen.SetActive(true);
        }

        void DisableDeathScreen()
        {
            deathScreen.SetActive(false);
        }

        public void KillMyself()
        {
            currentHealth = 0;
        }

        [RPC]
        void UpdateMyHealth(float hp)
        {
            currentHealth = hp;
        }

        [RPC]
        void Die()
        {
            controller.IsAlive = false;
            isAlive = false;
        }

        [RPC]
        void AddForceFromPush(float power, Vector3 origin)
        {
            pushPower = power;
            pushed = true;
            pushOrigin = transform.position - origin;
            pushOrigin.Normalize();
        }

        [RPC]
        void TellIBlockedHit()
        {
            blocked = true;
        }
    }
}