﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class PlayerSetup : MonoBehaviour
    {
        private NetworkView netView;
        private bool initialSetup = false;

        //VARIABLES

        private Transform myTransform;
        private Transform myCamera;

        private PlayerController controller;
        private MouseController mouse;

        public Transform localMesh;
        public Transform remoteMesh;

        private MultiplayerManager mg;

        private bool disabled;

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
            myTransform = transform;

            controller = GetComponent<PlayerController>();

            mg = GameObject.FindGameObjectWithTag("Managers").GetComponent<MultiplayerManager>();
        }

        void InitialSetup()
        {
            if (mg.MapLoaded && !initialSetup && netView.isMine)
            {
                myCamera = Camera.main.transform;
                localMesh.gameObject.layer = 8;
                localMesh.GetComponentInChildren<SkinnedMeshRenderer>().gameObject.layer = 8;
                remoteMesh.gameObject.layer = 9;
                controller.enabled = true;
                myCamera.SetParent(transform);
                myCamera.localPosition = new Vector3(0, 3.5f, 0.25f);
                myCamera.localRotation = Quaternion.EulerAngles(Vector3.zero);
                GameManager.instance.MyPlayer = gameObject;

                mouse = myCamera.gameObject.AddComponent<MouseController>();
                controller.Mouse = mouse;

                localMesh.SetParent(myCamera);

                netView.RPC("TellMyIdentityToTheServer", RPCMode.AllBuffered, Network.player, netView.viewID, mg.PlayerName);

                initialSetup = true;
            }
        }

        void Start()
        {
            if(netView.isMine)
            {
                InitialSetup();
            }
            else
            {
                localMesh.gameObject.SetActive(false);
                remoteMesh.gameObject.layer = 8;
                enabled = false;
            }
        }

        void Update()
        {
            if(!initialSetup)
            {
                InitialSetup();
            }
        }

        [RPC]
        void TellMyIdentityToTheServer(NetworkPlayer np, NetworkViewID id, string name)
        {
            MultiplayerDatabase db;
            db = GameObject.FindGameObjectWithTag("Managers").GetComponent<MultiplayerDatabase>();

            for (int i = 0; i < db.Players.Count; i++)
            {
                if (db.Players[i].NetworkPlayer == int.Parse(np.ToString()))
                {
                    gameObject.name = "Player " + np.ToString();
                    db.Players[i].Vis = gameObject;
                    db.Players[i].NetViewId = id;
                    db.Players[i].PlayerName = name;
                }
            }
        }

        public void DisableControls()
        {
            if (!disabled)
            {
                mouse.enabled = false;
                localMesh.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
                disabled = true;
            }
        }
    }
}