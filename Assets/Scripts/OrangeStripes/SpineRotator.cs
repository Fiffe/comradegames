﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class SpineRotator : MonoBehaviour
    {
        private NetworkView netView;

        //VARIABLES

        private Animator anim;
        private PlayerController controller;
        private PlayerMetabolism metabolism;

        public Quaternion currentSpineRot;
        public Quaternion currentNeckRot;

        private Transform spine;
        private Transform neck;

        void Awake()
        {
            netView = GetComponent<NetworkView>();
            anim = GetComponent<Animator>();
            metabolism = GetComponent<PlayerMetabolism>();
            spine = anim.GetBoneTransform(HumanBodyBones.Spine);
            neck = anim.GetBoneTransform(HumanBodyBones.Chest);
        }

        void Start()
        {
            if(netView.isMine)
            {
                controller = GetComponent<PlayerController>();
            }
        }

        void Update()
        {
            if(netView.isMine && metabolism.IsAlive)
            {
                currentSpineRot = controller.spineRot;
                currentNeckRot = controller.neckRot;

                netView.RPC("UpdateSpineRotation", RPCMode.Others, netView.viewID, currentSpineRot);
                netView.RPC("UpdateNeckRotation", RPCMode.Others, netView.viewID, currentNeckRot);
            }
        }

        void LateUpdate()
        {
            if (!netView.isMine && metabolism.IsAlive)
            {
                spine.localRotation = currentSpineRot;
                neck.localRotation = currentNeckRot;
            }
        }

        [RPC]
        public void UpdateSpineRotation(NetworkViewID id, Quaternion rot)
        {
            if (id == netView.viewID)
            {
                SpineRotator rotator = NetworkView.Find(id).GetComponent<SpineRotator>();

                if (Quaternion.Angle(rotator.currentSpineRot, rot) > 1)
                {
                    rotator.currentSpineRot = rot;
                }
            }
        }

        [RPC]
        void UpdateNeckRotation(NetworkViewID id, Quaternion rot)
        {
            if (id == netView.viewID)
            {
                SpineRotator rotator = NetworkView.Find(id).GetComponent<SpineRotator>();

                if (Quaternion.Angle(rotator.currentNeckRot, rot) > 1)
                {
                    rotator.currentNeckRot = rot;
                }
            }
        }
    }
}