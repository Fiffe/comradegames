﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class Weapon : Item
    {
        public EWeaponType WeaponType { get; set; }
        public int Damage { get; set; }
        public float Reach { get; set; }
        public float Speed { get; set; }
        public EDamageType DamageType { get; set; }

        public Weapon(EWeaponType weaponType, int damage, string reach, string speed, int id, EItemType type, EDamageType dmgType, string name, string desc, bool stack, int amount, string slug)
        {
            this.WeaponType = weaponType;
            this.DamageType = dmgType;
            this.Damage = damage;
            this.Reach = float.Parse(reach);
            this.Speed =  float.Parse(speed);
            this.Id = id;
            this.Type = type;
            this.Name = name;
            this.Description = desc;
            this.Stackable = stack;
            this.Amount = amount;
            this.Slug = slug;
            this.Sprite = Resources.Load<Sprite>("ItemIcons/" + slug);
            this.Prefab = Resources.Load<GameObject>("Prefabs/Items/" + type + "/" + Slug);
        }
    }
}