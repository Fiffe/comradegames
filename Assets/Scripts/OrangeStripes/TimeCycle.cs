﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class TimeCycle : MonoBehaviour
    {
        //VARIABLES

        private float dayLength = 1440;

        private float currentTime = 0.5f;

        private float multiplier = 1f;

        private float initialIntensity;
        private float initialAmbient;
        private float outputIntensity;

        private Light sun;

        //PROPERTIES

        public float DayLength
        {
            get
            {
                return dayLength;
            }
            set
            {
                dayLength = value;
            }
        }

        public float CurrentTime
        {
            get
            {
                return currentTime;
            }
            set
            {
                currentTime = value;
            }
        }

        //FUNCTIONS

        void Start()
        {
            sun = GetComponent<Light>();
            initialIntensity = sun.intensity;
            initialAmbient = RenderSettings.ambientIntensity;
        }

        void Update()
        {
            sun.transform.localRotation = Quaternion.Euler((currentTime * 360f) - 90, 170, 0);

            float intensityMultiplier = 1;
            float ambientMultiplier = 1;
            if(currentTime <= 0.23f || currentTime >= 0.75f)
            {
                intensityMultiplier = 0;
            }
            else if(currentTime <= 0.25f)
            {
                intensityMultiplier = Mathf.Clamp01((currentTime - 0.23f) * (1 / 0.02f));
            }
            else if(currentTime >= 0.73f)
            {
                intensityMultiplier = Mathf.Clamp01(1 - ((currentTime - 0.73f) * (1 / 0.02f)));
            }

            ambientMultiplier = Mathf.Clamp(intensityMultiplier, 0.4f, 1f);

            outputIntensity = Mathf.Clamp(initialIntensity * intensityMultiplier + 0.05f, 0.05f, 0.4f);
            
            sun.intensity = outputIntensity;
            RenderSettings.ambientIntensity = initialAmbient * ambientMultiplier;

            currentTime += (Time.deltaTime / dayLength) * multiplier;

            if(currentTime >= 1)
            {
                currentTime = 0;
            }
        }
    }
}