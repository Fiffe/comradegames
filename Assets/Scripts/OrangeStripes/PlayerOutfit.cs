﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class PlayerOutfit : MonoBehaviour
    {
        private NetworkView netView;
        private bool initialSetup;

        //VARIABLES

        private CharacterCustomization customization;

        private string currentHeadSlug = "";
        private string currentTorsoSlug = "";
        private string currentLegsSlug = "";
        private string currentFeetSlug = "";

        private GameObject currentHeadVis;
        private GameObject currentTorsoVis;
        private GameObject currentLegsVis;
        private GameObject currentFeetVis;

        private bool alreadySent = false;

        //PROPERTIES

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
        }

        void InitialSetup()
        {
            if (GameManager.instance)
            {
                if (GameManager.instance.MultiplayerManager)
                {
                    if (GameManager.instance.MultiplayerManager.MapLoaded)
                    {
                        customization = GameManager.instance.CharCustomization;
                        initialSetup = true;
                    }
                }
            }
        }

        void Start()
        {
            if (!initialSetup)
            {
                InitialSetup();
            }
        }

        void Update()
        {
            if (!initialSetup)
            {
                InitialSetup();
            }

            if (initialSetup)
            {
                if (netView.isMine)
                {
                    AssignValues();
                    ControlOutfits();
                    if(!alreadySent)
                    {
                        netView.RPC("TellMyOutfit", RPCMode.OthersBuffered, netView.viewID, currentHeadSlug, currentTorsoSlug, currentLegsSlug, currentFeetSlug);
                        alreadySent = true;
                    }
                }
                else
                {
                    ControlOutfits();
                }
            }
        }

        void AssignValues()
        {
            if (customization.currentHead)
            {
                currentHeadSlug = customization.currentHead.name;
            }

            if (customization.currentTorso)
            {
                currentTorsoSlug = customization.currentTorso.name;
            }

            if (customization.currentLegs)
            {
                currentLegsSlug = customization.currentLegs.name;
            }

            if (customization.currentFeet)
            {
                currentFeetSlug = customization.currentFeet.name;
            }
        }

        void ControlOutfits()
        {
            if (!currentHeadVis)
            {
                if (currentHeadSlug != "")
                {
                    EquipOutfit("head", currentHeadSlug);
                }
            }

            if (!currentTorsoVis)
            {
                if (currentTorsoSlug != "")
                {
                    EquipOutfit("torso", currentTorsoSlug);
                }
            }

            if (!currentLegsVis)
            {
                if (currentLegsSlug != "")
                {
                    EquipOutfit("legs", currentLegsSlug);
                }
            }

            if (!currentFeetVis)
            {
                if (currentFeetSlug != "")
                {
                    EquipOutfit("feet", currentFeetSlug);
                }
            }
        }

        void EquipOutfit(string type, string slug)
        {
            switch (type)
            {
                case "head":
                    foreach (GameObject go in customization.Head)
                    {
                        if (go.name == slug)
                        {
                            GameObject it = (GameObject)Instantiate(go, Vector3.zero, Quaternion.identity);

                            Vector3 equipPos = it.GetComponent<Outfit>().equipPos;
                            Vector3 equipRot = it.GetComponent<Outfit>().equipRot;

                            it.transform.SetParent(transform.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head), false);
                            it.transform.localPosition = equipPos;
                            it.transform.localRotation = Quaternion.Euler(equipRot);
                            if (netView.isMine)
                            {
                                it.layer = 9;
                                foreach (Transform tr in it.transform)
                                {
                                    tr.gameObject.layer = 9;
                                }
                            }
                            else
                            {
                                it.layer = 8;
                                foreach (Transform tr in it.transform)
                                {
                                    tr.gameObject.layer = 8;
                                }
                            }
                            currentHeadVis = it;
                        }
                    }
                    break;
                case "torso":
                    foreach (GameObject go in customization.Torso)
                    {
                        if (go.name == slug)
                        {
                            GameObject it = (GameObject)Instantiate(go, Vector3.zero, Quaternion.identity);

                            it.transform.SetParent(transform, false);
                            if (netView.isMine)
                            {
                                it.layer = 9;
                                foreach (Transform tr in it.transform)
                                {
                                    tr.gameObject.layer = 9;
                                }
                            }
                            else
                            {
                                it.layer = 8;
                                foreach (Transform tr in it.transform)
                                {
                                    tr.gameObject.layer = 8;
                                }
                            }
                            currentTorsoVis = it;
                        }
                    }
                    break;
                case "legs":
                    foreach (GameObject go in customization.Legs)
                    {
                        if (go.name == slug)
                        {
                            GameObject it = (GameObject)Instantiate(go, Vector3.zero, Quaternion.identity);

                            it.transform.SetParent(transform, false);
                            if (netView.isMine)
                            {
                                it.layer = 9;
                                foreach (Transform tr in it.transform)
                                {
                                    tr.gameObject.layer = 9;
                                }
                            }
                            else
                            {
                                it.layer = 8;
                                foreach (Transform tr in it.transform)
                                {
                                    tr.gameObject.layer = 8;
                                }
                            }
                            currentLegsVis = it;
                        }
                    }
                    break;
                case "feet":
                    foreach (GameObject go in customization.Feet)
                    {
                        if (go.name == slug)
                        {
                            GameObject it = (GameObject)Instantiate(go, Vector3.zero, Quaternion.identity);

                            it.transform.SetParent(transform, false);
                            if (netView.isMine)
                            {
                                it.layer = 9;
                                foreach (Transform tr in it.transform)
                                {
                                    tr.gameObject.layer = 9;
                                }
                            }
                            else
                            {
                                it.layer = 8;
                                foreach (Transform tr in it.transform)
                                {
                                    tr.gameObject.layer = 8;
                                }
                            }
                            currentFeetVis = it;
                        }
                    }
                    break;
            }
        }

        [RPC]
        void TellMyOutfit(NetworkViewID id, string head, string torso, string legs, string feet)
        {
            if (netView.viewID == id)
            {
                currentHeadSlug = head;
                currentTorsoSlug = torso;
                currentLegsSlug = legs;
                currentFeetSlug = feet;
            }
        }
    }
}