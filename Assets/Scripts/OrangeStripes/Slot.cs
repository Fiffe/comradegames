﻿using UnityEngine;

namespace OrangeStripes
{
    public class Slot : MonoBehaviour
    {
        //VARIABLES

        private int id;

        private Hotbar hotbar;

        //PROPERTIES

        public int ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
    }
}