﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class Item
    {
        public int Id { get; set; }
        public EItemType Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Stackable { get; set; }
        public int Amount { get; set; }
        public string Slug { get; set; }
        public Sprite Sprite { get; set; }
        public GameObject Prefab { get; set; }

        public Item(int id, EItemType type, string name, string desc, bool stack, int amount, string slug)
        {
            this.Id = id;
            this.Type = type;
            this.Name = name;
            this.Description = desc;
            this.Stackable = stack;
            this.Amount = amount;
            this.Slug = slug;
            this.Sprite = Resources.Load<Sprite>("ItemIcons/" + slug);
            this.Prefab = Resources.Load<GameObject>("Prefabs/Items/" + type + "/" + Slug);
        }

        public Item()
        {
            this.Id = -1;
        }
    }
}