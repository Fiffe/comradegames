﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace OrangeStripes
{
    public class Progress : MonoBehaviour
    {
        private NetworkView netView;
        private bool initialSetup = false;

        //VARIABLES

        private MultiplayerManager mg;
        private GameObject progressObj;
        private Image progressBar;

        public bool isInteracting = false;
        public bool done = false;

        private float interactionTime;
        private float interactionTimer;

        //PROPERTIES

        public float InteractionTime
        {
            set
            {
                interactionTime = value;
            }
        }

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
        }

        void InitialSetup()
        {
            if(mg && !initialSetup)
            {
                if(mg.MapLoaded)
                {
                    progressObj = GameObject.Find("ProgressBG");
                    progressBar = progressObj.transform.GetChild(0).GetComponent<Image>();
                    initialSetup = true;
                }
            }
        }

        void Start()
        {
            if (netView.isMine)
            {
                if (GameManager.instance)
                {
                    if (GameManager.instance.MultiplayerManager)
                    {
                        mg = GameManager.instance.MultiplayerManager;
                    }
                }

                InitialSetup();
                
            }
            else
            {
                enabled = false;
            }
        }

        void Update()
        {
            if(!mg && netView.isMine)
            {
                if (GameManager.instance)
                {
                    if (GameManager.instance.MultiplayerManager)
                    {
                        mg = GameManager.instance.MultiplayerManager;
                    }
                }
            }

            if(netView.isMine)
            {
                if(!initialSetup)
                {
                    InitialSetup();
                }
            }

            if (initialSetup)
            {
                if (isInteracting)
                {
                    if (!progressObj.activeSelf)
                    {
                        progressObj.SetActive(true);
                    }

                    interactionTimer += Time.deltaTime;

                    progressBar.fillAmount = interactionTimer / interactionTime;

                    if (interactionTimer >= interactionTime)
                    {
                        done = true;
                    }
                }
                else
                {
                    if (progressObj.activeSelf)
                    {
                        progressObj.SetActive(false);
                    }

                    interactionTimer = 0;
                }
            }
        }
    }
}