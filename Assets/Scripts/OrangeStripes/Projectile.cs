﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class Projectile : MonoBehaviour
    {
        //VARIABLES

        public int damage;

        private float speed = 60;
        private RaycastHit hit;
        private float rayDistance = 1f;
        private bool expended;
        private float expireTime = 30;
        private string owner;
        private LayerMask mask;

        //PROPERTIES

        public string Owner
        {
            get
            {
                return owner;
            }
            set
            {
                owner = value;
            }
        }

        //FUNCTIONS

        void Start()
        {
            mask = ~(1 << LayerMask.NameToLayer("Ignore Raycast") | 1 << LayerMask.NameToLayer("Player"));
            StartCoroutine(DestroyMyself());
        }

        void Update()
        {
            if (!expended)
            {
                transform.Translate(-Vector3.forward * speed * Time.deltaTime);
            }

            if(Physics.Raycast(transform.position, -transform.forward, out hit, rayDistance, mask) && !expended)
            {
                if(hit.collider)
                {
                    if(hit.collider.GetComponentInParent<PlayerMetabolism>())
                    {
                        hit.collider.GetComponentInParent<PlayerMetabolism>().Damage(damage, EDamageType.Bullet, hit.collider.name);
                        hit.collider.GetComponentInParent<PlayerMetabolism>().GetPushed(500, transform.position);
                        transform.SetParent(hit.collider.transform);
                    }

                    expended = true;
                }                
            }
        }

        IEnumerator DestroyMyself()
        {
            yield return new WaitForSeconds(expireTime);
            Destroy(gameObject);
        }
    }
}