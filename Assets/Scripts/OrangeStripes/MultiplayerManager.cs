﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace OrangeStripes
{
    public class MultiplayerManager : MonoBehaviour
    {
        private NetworkView netView;
        private static MultiplayerManager instance;

        //VARIABLES

        private string ipAddress = "127.0.0.1";
        private int port = 26500;
        private string serverName = "Server";
        private bool useNat;
        private string playerName = "Unknown Player";
        private string password = "";

        private int maxPlayers = 16;

        [Header("Menus")]

        public Image background;
        public GameObject mainMenuCanvas;
        public GameObject loginMenu;
        public GameObject registerMenu;
        public GameObject mainMenu;
        public GameObject hostMenu;
        public GameObject connectMenu;
        public GameObject serverMenu;
        public GameObject clientMenu;
        public GameObject customizationMenu;
        public GameObject itemListMenu;

        [Header("Input Fields")]

        public InputField loginUsername;
        public InputField loginPassword;
        public InputField registerUsername;
        public InputField registerPassword;
        public InputField hostIpAddress;
        public InputField hostServerName;
        public InputField hostPort;
        public InputField connectIpAddress;
        public InputField connectPort;

        [Header("Buttons")]

        public Button loginButton;
        public Button registerButton;
        public Button hostButton;
        public Button connectButton;
        public Button startButton;
        public Button shutdownButton;
        public Button disconnectButton;
        public Button lobbyStart;

        [Header("Texts")]

        public Text mainTitle;
        public Text serverServerName;
        public Text serverPlayers;
        public Text clientServerName;
        public Text clientPlayers;

        private GameObject[] menus = new GameObject[9];

        private int selectedMenuId = 0;
        private int currentMenuId = -1;

        private bool showWindow;
        private bool toggleWindow;

        private TimeCycle cycle;
        private float currentTime;

        private bool setup;
        private bool changeTime;

        private bool loggedIn = false;

        private bool mapLoaded = false;
        private bool gameStarted = false;

        [Header("Lobbies")]

        private List<PlayerData> Players = new List<PlayerData>();
        private List<GameObject> LobbySlots = new List<GameObject>();
        private GameObject lobbySlotPrefab;

        public GameObject serverLobby;
        public GameObject clientLobby;

        [Header("Customization")]

        public CharacterCustomization customization;

        //PROPERTIES

        public string PlayerName
        {
            get
            {
                return playerName;
            }
            set
            {
                playerName = value;
            }
        }

        public bool UseNAT
        {
            get
            {
                return useNat;
            }
            set
            {
                useNat = value;
            }
        }

        public string ServerName
        {
            get
            {
                return serverName;
            }
            set
            {
                serverName = value;
            }
        }

        public bool ShowWindow
        {
            get
            {
                return showWindow;
            }
            set
            {
                showWindow = value;
            }
        }

        public bool MapLoaded
        {
            get
            {
                return mapLoaded;
            }
        }

        public bool GameStarted
        {
            get
            {
                return gameStarted;
            }
        }
        
        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();

            if(!instance)
            {
                DontDestroyOnLoad(gameObject);
                instance = this;
            }
            else if(instance != this)
            {
                Destroy(gameObject);
            }
        }

        void Start()
        {
            menus[0] = mainMenu;
            menus[1] = hostMenu;
            menus[2] = connectMenu;
            menus[3] = serverMenu;
            menus[4] = clientMenu;
            menus[5] = loginMenu;
            menus[6] = registerMenu;
            menus[7] = customizationMenu;
            menus[8] = itemListMenu;

            lobbySlotPrefab = Resources.Load<GameObject>("Prefabs/UI/LobbySlot");

            serverName = PlayerPrefs.GetString("MyServerName");
            if (serverName == "")
            {
                serverName = "Server";
            }

            SetupInputFields();
            DisableTitleScreenMenus();
        }

        void Update()
        {

            if (Network.peerType == NetworkPeerType.Disconnected)
            {
                if(Cursor.lockState == CursorLockMode.Locked)
                {
                    Cursor.lockState = CursorLockMode.None;
                }

                if(!Cursor.visible)
                {
                    Cursor.visible = true;
                }

                if (currentMenuId != selectedMenuId)
                {
                    ToggleMenu(selectedMenuId);
                    currentMenuId = selectedMenuId;
                }

                if(gameStarted)
                {
                    gameStarted = false;
                }

                ControlMainMenuButtons();
            }
            else
            {
                DisableTitleScreenMenus();
            }

            if(Network.peerType == NetworkPeerType.Server)
            {
                if (mapLoaded)
                {
                    if (!GameManager.instance.MultiplayerManager)
                    {
                        GameManager.instance.MultiplayerManager = instance;
                    }
                }

                if (currentMenuId != 3)
                {
                    ToggleMenu(3);
                    currentMenuId = 3;
                }

                if(setup)
                {
                    cycle = GameObject.FindGameObjectWithTag("Sun").GetComponent<TimeCycle>();

                    setup = false;
                }

                if(cycle)
                {
                    currentTime = cycle.CurrentTime;
                }

                if(currentMenuId == 3)
                {
                    if(gameStarted)
                    {
                        lobbyStart.interactable = false;
                    }
                    else
                    {
                        lobbyStart.interactable = true;
                    }
                }

                ControlServerInfo();
            }

            if(Network.peerType == NetworkPeerType.Client && mapLoaded)
            {
                if (!GameManager.instance.MultiplayerManager)
                {
                    GameManager.instance.MultiplayerManager = instance;
                }

                if (toggleWindow)
                {
                    ToggleMenu(4);
                    currentMenuId = 4;
                    toggleWindow = false;
                }
                
                if(setup)
                {
                    cycle = GameObject.FindGameObjectWithTag("Sun").GetComponent<TimeCycle>();

                    netView.RPC("TellMyName", RPCMode.AllBuffered, Network.player, playerName);

                    setup = false;
                }

                if (cycle)
                {
                    if (changeTime)
                    {
                        ChangeTime(currentTime);
                        changeTime = false;
                    }
                }

                if(!showWindow)
                {
                    DisableInGameMenus();
                }

                ControlClientInfo();

                if (Input.GetButtonDown("Menu"))
                {
                    toggleWindow = !toggleWindow;
                    showWindow = !showWindow;
                }
            }
            
            if(currentMenuId != 7 && currentMenuId != 8)
            {
                customization.DisablePreview();
            }
        }

        public void ToggleNAT(bool b)
        {
            useNat = b;
        }

        public void HostServer()
        {
            serverServerName.text = "SERVER NAME: " + serverName.ToUpper();
            Network.InitializeServer(maxPlayers, port, useNat);
            StartCoroutine(LoadMap(1));
        }

        public void ConnectToServer()
        {
            Network.Connect(ipAddress, port);
            StartCoroutine(LoadMap(1));
        }

        public void Disconnect()
        {
            Network.Disconnect();
        }

        void SetupInputFields()
        {
            hostIpAddress.text = ipAddress;
            connectIpAddress.text = ipAddress;
            hostServerName.text = serverName;            
        }

        void ControlMainMenuButtons()
        {
            if(currentMenuId == 1)
            {
                if(serverName != "" && ipAddress != "" && port != 0)
                {
                    hostButton.interactable = true;
                }
                else
                {
                    hostButton.interactable = false;
                }
            }

            if(currentMenuId == 2)
            {
                if(playerName != "" && ipAddress != "" && port != 0)
                {
                    connectButton.interactable = true;
                }
                else
                {
                    connectButton.interactable = true;
                }
            }
        }

        void ControlServerInfo()
        {
            serverPlayers.text = "CONNECTED PLAYERS: " + Network.connections.Length;
        }

        void ControlClientInfo()
        {
            clientPlayers.text = "CONNECTED PLAYERS: " + Network.connections.Length;
        }

        public void UpdateIPAddress(string value)
        {
            ipAddress = value;
        }

        public void UpdatePort(string value)
        {
            if (value != "")
            {
                port = int.Parse(value);
            }
        }

        public void UpdateServerName(string value)
        {
            PlayerPrefs.SetString("ServerName", value);
            serverName = value;
        }

        void ToggleMenu(int id)
        {
            if (!background.isActiveAndEnabled && Network.peerType == NetworkPeerType.Disconnected)
            {
                background.enabled = true;
                mainTitle.enabled = true;
            }

            for (int i = 0; i < menus.Length; i++)
            {
                if (i == id)
                {
                    menus[i].SetActive(true);
                }
                else
                {
                    menus[i].SetActive(false);
                }
            }
        }

        public void ChangeMenu(string id)
        {
            selectedMenuId = int.Parse(id);
        }

        void DisableTitleScreenMenus()
        {
            for (int i = 0; i < 3; i++)
            {
                if (menus[i].activeInHierarchy)
                {
                    menus[i].SetActive(false);
                }

                if(i == 2)
                {
                    if (loggedIn)
                    {
                        selectedMenuId = 0;
                    }
                    else
                    {
                        selectedMenuId = 5;
                    }

                    currentMenuId = -1;

                    if (background.isActiveAndEnabled)
                    {
                        background.enabled = false;
                        mainTitle.enabled = false;
                    }
                }
            }
        }

        void DisableInGameMenus()
        {
            for (int i = 3; i < menus.Length; i++)
            {
                if (menus[i].activeInHierarchy)
                {
                    menus[i].SetActive(false);
                }

                if (i == menus.Length - 1)
                {
                    selectedMenuId = 0;
                    currentMenuId = -1;
                }
            }
        }

        void OnPlayerConnected(NetworkPlayer netPlayer)
        {
            netView.RPC("TellServerName", netPlayer, serverName);
            netView.RPC("TellServerTime", netPlayer, currentTime);
            netView.RPC("AddToLobby", RPCMode.AllBuffered, netPlayer);
        }

        void OnPlayerDisconnected(NetworkPlayer netPlayer)
        {
            netView.RPC("RemoveFromLobby", RPCMode.AllBuffered, netPlayer);
        }

        [RPC]
        void TellMyName(NetworkPlayer netPlayer, string s)
        {
            for(int i = 0; i < Players.Count; i++)
            {
                if(Players[i].NetworkPlayer == int.Parse(netPlayer.ToString()))
                {
                    Players[i].PlayerName = s;
                }
            }

            for(int i = 0; i < LobbySlots.Count; i++)
            {
                PlayerData _data = LobbySlots[i].GetComponent<ScoreSlot>().data;

                if(_data.NetworkPlayer == int.Parse(netPlayer.ToString()))
                {
                    LobbySlots[i].GetComponent<ScoreSlot>().data.PlayerName = s;
                    LobbySlots[i].GetComponent<Text>().text = s;
                }
            }
        }

        [RPC]
        void AddToLobby(NetworkPlayer np)
        {
            PlayerData _data = new PlayerData();
            _data.NetworkPlayer = int.Parse(np.ToString());

            Players.Add(_data);

            GameObject slot = Instantiate(lobbySlotPrefab);
            LobbySlots.Add(slot);
            slot.GetComponent<ScoreSlot>().data = _data;
            slot.GetComponent<Text>().text = _data.PlayerName;

            if (Network.peerType == NetworkPeerType.Server)
            {
                slot.transform.SetParent(serverLobby.transform, false);
            }
            else
            {
                slot.transform.SetParent(clientLobby.transform, false);
            }
        }

        [RPC]
        void RemoveFromLobby(NetworkPlayer np)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                if (Players[i].NetworkPlayer == int.Parse(np.ToString()))
                {
                    Destroy(LobbySlots[i]);
                    Players.RemoveAt(i);
                    LobbySlots.RemoveAt(i);
                }
            }
        }

        void OnDisconnectedFromServer()
        {
            Application.LoadLevel(0);
            ResetLobbies();
        }

        void ResetLobbies()
        {
            Players.Clear();

            foreach(Transform trs in clientLobby.transform)
            {
                Destroy(trs.gameObject);
            }

            foreach(Transform trs in serverLobby.transform)
            {
                Destroy(trs.gameObject);
            }

            LobbySlots.Clear();
        }

        void ChangeTime(float time)
        {
            cycle.CurrentTime = time;
        }

        [RPC]
        void TellServerName(string name)
        {
            serverName = name;
            clientServerName.text = "SERVER NAME: " + name.ToUpper();
        }

        [RPC]
        void TellServerTime(float time)
        {
            currentTime = time;
            changeTime = true;
        }

        public void Quit()
        {
            if (!Application.isEditor)
            {
                Application.Quit();
            }
        }

        void OnFailedToConnect(NetworkConnectionError error)
        {
            Debug.Log("Could not connect to server: " + error);
            Application.LoadLevel(0);
        }

        public void StartTheGame()
        {
            netView.RPC("GameStart", RPCMode.AllBuffered);
            Network.maxConnections = Players.Count;
            Debug.Log(Network.maxConnections);
        }

        [RPC]
        void GameStart()
        {
            gameStarted = true;
        }

        IEnumerator LoadMap(int id)
        {
            Network.SetSendingEnabled(0, false);
            Network.isMessageQueueRunning = false;
            Network.SetLevelPrefix(1);

            AsyncOperation async = Application.LoadLevelAsync(id);
            yield return async;

            setup = true;
            mapLoaded = true;
            Network.isMessageQueueRunning = true;
            Network.SetSendingEnabled(0, true);
        }

        public void GenerateLoggingLink()
        {
            if (loginUsername.text != "dev" && loginPassword.text != "dev")
            {
                WWWForm logForm = new WWWForm();
                logForm.AddField("login", loginUsername.text);
                logForm.AddField("pass", loginPassword.text);
                playerName = loginUsername.text;
                WWW link = new WWW("http://www.fiffe.boo.pl/login.php", logForm);
                StartCoroutine(Logging(link));
            }
            else
            {
                loggedIn = true;
                ToggleMenu(0);
            }
        }

        public void GenerateRegisteringLink()
        {
            WWWForm regForm = new WWWForm();
            regForm.AddField("login", registerUsername.text);
            regForm.AddField("pass", registerPassword.text);
            WWW link = new WWW("http://www.fiffe.boo.pl/register.php", regForm);
            StartCoroutine(Registering(link));
        }

        IEnumerator Logging(WWW link)
        {
            yield return link;
            Debug.Log(link.text);
            if(link.text == "success")
            {
                loggedIn = true;
                ToggleMenu(0);
            }
        }

        IEnumerator Registering(WWW link)
        {
            yield return link;
            if(link.text == "success")
            {
                ToggleMenu(5);
            }
        }
    }
}
