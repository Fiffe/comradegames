﻿using UnityEngine;
using System.Collections;

public class MenuCanvas : MonoBehaviour {

    private static Canvas instance;

    void Awake()
    {
        if(!instance)
        {
            DontDestroyOnLoad(gameObject);
            instance = GetComponent<Canvas>();
        }
        else if(instance != GetComponent<Canvas>())
        {
            Destroy(gameObject);
        }
    }

    void OnLevelWasLoaded(int level)
    {
        instance.worldCamera = Camera.main;
    }
}
