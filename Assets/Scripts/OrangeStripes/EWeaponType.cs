﻿using System;

namespace OrangeStripes
{
    public enum EWeaponType
    {
        Fists,
        OneHanded,
        TwoHanded,
        Bow
    }
}