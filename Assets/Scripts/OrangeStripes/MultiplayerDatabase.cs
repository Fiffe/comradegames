﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace OrangeStripes
{
    public class MultiplayerDatabase : MonoBehaviour
    {
        private NetworkView netView;

        //VARIABLES

        public List<PlayerData> Players = new List<PlayerData>();
        private NetworkPlayer netPlayer;

        private bool spawned;

        //PROPERTIES

        public NetworkPlayer NetPlayer
        {
            get
            {
                return netPlayer;
            }
            set
            {
                netPlayer = value;
            }
        }

        public bool Spawned
        {
            get
            {
                return spawned;
            }
            set
            {
                spawned = value;
            }
        }

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
        }

        void OnPlayerConnected(NetworkPlayer np)
        {
            netView.RPC("AddPlayerToList", RPCMode.AllBuffered, np);
        }

        void OnPlayerDisconnected(NetworkPlayer np)
        {
            netView.RPC("RemovePlayerFromList", RPCMode.AllBuffered, np);
        }

        void OnDisconnectedFromServer()
        {
            Players.Clear();
        }

        public PlayerData FetchPlayer(NetworkPlayer np)
        {
            for(int i = 0; i < Players.Count; i++)
            {
                if(Players[i].NetworkPlayer == int.Parse(np.ToString()))
                {
                    return Players[i];
                }
            }

            return null;
        }

        [RPC]
        void AddPlayerToList(NetworkPlayer np)
        {
            PlayerData data = new PlayerData();
            data.NetworkPlayer = int.Parse(np.ToString());

            Players.Add(data);
        }

        [RPC]
        void RemovePlayerFromList(NetworkPlayer np)
        {
            for(int i = 0; i < Players.Count; i++)
            {
                if(Players[i].NetworkPlayer == int.Parse(np.ToString()))
                {
                    Players.RemoveAt(i);
                }
            }
        }
    }
}