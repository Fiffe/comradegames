﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public interface IDamageable
    {
        void Damage(float damage, EDamageType type, string limb);

        void Bleed(float power);
    }
}