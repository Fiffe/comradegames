﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace OrangeStripes
{
    public class DevAnimTest : MonoBehaviour
    {
        public List<string> Clips = new List<string>();
        public Animator anim;

        public GameObject buttonPrefab;

        void Awake()
        {
            anim = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
        }

        void Start()
        {
            foreach (AnimationClip clip in anim.runtimeAnimatorController.animationClips)
            {
                Clips.Add(clip.name);
            }

            GenerateButtons();
        }

        void GenerateButtons()
        {
            for(int i = 0; i < Clips.Count; i++)
            {
                int x = i;
                GameObject btn = Instantiate(buttonPrefab, Vector3.zero, Quaternion.identity) as GameObject;
                btn.transform.SetParent(transform.GetChild(0).GetComponent<ScrollRect>().content.transform);
                btn.transform.localScale = new Vector3(1, 1, 1);
                btn.GetComponentInChildren<Text>().text = Clips[i];
                btn.GetComponent<Button>().onClick.AddListener(() => PlayAnim(Clips[x]));
            }

            PlayAnim(Clips[0]);
        }

        public void PlayAnim(string name)
        {
            anim.Play(name, 0);
        }
    }
}