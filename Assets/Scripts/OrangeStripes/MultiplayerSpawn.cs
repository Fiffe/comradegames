﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class MultiplayerSpawn : MonoBehaviour
    {
        private bool initialSetup = false;

        //VARIABLES

        private bool isSpawned;

        public GameObject playerPrefab;

        private GameObject[] spawns;

        private MultiplayerDatabase db;
        private MultiplayerManager mg;
        private GameManager gm;

        //FUNCTIONS

        void Awake()
        {
            DontDestroyOnLoad(this);
        }

        void Start()
        {
            InitialSetup();
        }

        void InitialSetup()
        {
            if (!initialSetup)
            {
                db = GameObject.FindGameObjectWithTag("Managers").GetComponent<MultiplayerDatabase>();

                if (db)
                {
                    mg = db.GetComponent<MultiplayerManager>();
                }

                if(db && mg)
                {
                    initialSetup = true;
                }
            }
        }

        void OnConnectedToServer()
        {
            gm = null;

            SetupSpawnPoints();
            isSpawned = false;
        }

        void Update()
        {
            if(!initialSetup)
            {
                InitialSetup();
            }

            if(Network.peerType == NetworkPeerType.Client)
            {
                if (initialSetup)
                {
                    if(!gm)
                    {
                        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
                    }

                    if (mg.MapLoaded)
                    {
                        if (mg.GameStarted)
                        {
                            if (!isSpawned)
                            {
                                gm.EnableCanvases();
                                Spawn();
                            }
                        }
                    }
                }
            }
        }

        void SetupSpawnPoints()
        {
            spawns = GameObject.FindGameObjectsWithTag("Spawn");
        }

        public void Spawn()
        {
            if (spawns.Length > 0)
            {
                Transform randomSpawn = spawns[Random.Range(0, spawns.Length)].gameObject.transform;
                if (randomSpawn)
                {
                    Network.Instantiate(playerPrefab, randomSpawn.position, randomSpawn.rotation, 0);
                    isSpawned = true;
                }
            }
            else
            {
                SetupSpawnPoints();
            }
        }
    }
}