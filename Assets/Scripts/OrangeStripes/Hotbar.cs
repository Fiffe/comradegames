﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace OrangeStripes
{
    public class Hotbar : MonoBehaviour
    {
        private NetworkView netView;
        private bool initialSetup;

        //VARIABLES

        public string prefabsPath = "Prefabs/UI/";

        private GameObject myPlayer;
        private Animator myAnim;
        private ItemDatabase db;
        private MultiplayerDatabase mpDb;
        private MultiplayerManager mg;
        private PlayerController controller;

        private GameObject inventoryItem;
        private GameObject hotbar;
        private GameObject selector;
        private List<Slot> Slots = new List<Slot>();
        public List<Item> Items = new List<Item>();

        private GameObject equippedItem;

        private Vector3 equipPos = new Vector3(-0.222f, 0.046f, 0.056f);
        private Vector3 equipRot = new Vector3(283.3423f, 307.7732f, 63.80103f);

        private Vector3 bowEquipPos = new Vector3(-0.229f, -0.067f, 0.039f);
        private Vector3 bowEquipRot = new Vector3(6.866841f, 337.9705f, 179.1944f);

        private Vector3 arrowEquipPos = new Vector3(-0.158f, 0.062f, -0.094f);
        private Vector3 arrowEquipRot = new Vector3(9.825144f, 5.528864f, 231.2671f);

        RaycastHit itemHit;
        LayerMask itemMask;

        private int currentSelected = -1;

        //PROPERTIES

        public int CurrentSelected
        {
            get
            {
                return currentSelected;
            }
        }

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
        }

        void InitialSetup()
        {
            if (mg)
            {
                if (mg.MapLoaded && !initialSetup)
                {
                    inventoryItem = Resources.Load<GameObject>(prefabsPath + "Item");
                    myPlayer = gameObject;
                    controller = myPlayer.GetComponent<PlayerController>();
                    myAnim = GetComponent<PlayerSetup>().localMesh.GetComponent<Animator>();
                    GameObject.Find("InventoryCanvas").SetActive(true);
                    hotbar = GameObject.Find("HotbarPanel");                    
                    selector = GameObject.Find("Selector");
                    itemMask = ~(1 << LayerMask.NameToLayer("Default") | 1 << LayerMask.NameToLayer("Ignore Raycast") | 1 << LayerMask.NameToLayer("MyRagdolls") | 1 << LayerMask.NameToLayer("Player"));

                    for (int i = 0; i < hotbar.transform.childCount; i++)
                    {
                        Items.Add(new Item());
                        GameObject slt = hotbar.transform.GetChild(i).gameObject;
                        Slot hb = slt.AddComponent<Slot>();
                        Slots.Add(hb);
                        hb.ID = i;

                        Item itemToAdd = db.FetchItemById(0);
                        Items[i] = itemToAdd;
                        GameObject itemGO = Instantiate(inventoryItem);
                        itemGO.transform.SetParent(Slots[i].transform, false);
                        itemGO.GetComponent<Image>().sprite = itemToAdd.Sprite;
                        itemGO.name = itemToAdd.Slug;
                        ItemData data = itemGO.GetComponent<ItemData>();
                        data.Item = itemToAdd;
                        data.SlotID = i;
                        data.Amount = itemToAdd.Amount;
                        itemGO.transform.GetChild(0).GetComponent<Text>().text = "";
                    }

                    AddItem(3);
                    AddItem(2);
                    AddItem(4);
                    AddItem(5);

                    SelectSlot(0);

                    initialSetup = true;
                }
            }
        }

        void Start()
        {
            db = GameObject.FindGameObjectWithTag("Managers").GetComponent<ItemDatabase>();
            mpDb = GameObject.FindGameObjectWithTag("Managers").GetComponent<MultiplayerDatabase>();

            if (GameManager.instance)
            {
                if (GameManager.instance.MultiplayerManager)
                {
                    mg = GameManager.instance.MultiplayerManager;
                }
            }

            if (netView.isMine)
            {
                InitialSetup();
            }
            else
            {
                enabled = false;
            }
        }

        void Update()
        {
            if(!mg)
            {
                if (GameManager.instance)
                {
                    if (GameManager.instance.MultiplayerManager)
                    {
                        mg = GameManager.instance.MultiplayerManager;
                    }
                }
            }

            if(!initialSetup && netView.isMine)
            {
                InitialSetup();
            }

            if (Network.peerType != NetworkPeerType.Disconnected)
            {
                if (initialSetup)
                {
                    if (Cursor.lockState == CursorLockMode.Locked)
                    {
                        ControlButtons();
                    }

                    if (!hotbar.activeSelf)
                    {
                        DisableVis();
                    }

                    if (selector.activeSelf)
                    {
                        UpdateSelectorPosition();
                    }

                    if (currentSelected == -1 || !selector.activeSelf)
                    {
                        if (equippedItem)
                        {
                            Deequip();
                        }
                    }

                    ControlDropping();
                    ControlPickingUp();
                }
            }
        }

        EItemType CheckItemType(int i)
        {
            if(Items[i].Type == EItemType.Weapons)
            {
                return EItemType.Weapons;
            }

            return EItemType.Others;
        }

        void ControlButtons()
        {
            if(Input.GetKeyDown(KeyCode.Alpha1))
            {
                SelectSlot(0);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                SelectSlot(1);
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                SelectSlot(2);
            }

            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                SelectSlot(3);
            }

            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                SelectSlot(4);
            }

            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                SelectSlot(5);
            }
        }

        void CheckForEmptySlot(int i)
        {
            if (i != 0)
            {
                if (Items[i].Id == 0)
                {
                    if (selector.activeSelf)
                    {
                        DisableVis();
                    }

                    currentSelected = -1;
                }
                else if (Items[i].Id != 0)
                {
                    if(CheckItemType(i) != EItemType.Weapons)
                    {
                        DisableVis();
                    }
                }
            }
        }

        void SelectSlot(int i)
        {
            if (currentSelected != i)
            {
                Deequip();

                if (Items[i].Id != -1)
                {
                    if (!selector.activeSelf)
                    {
                        EnableVis();
                    }

                    currentSelected = i;
                    Equip(Items[currentSelected]);
                }
            }
        }

        void ControlDropping()
        {
            if (equippedItem)
            {
                if (equippedItem.GetComponent<ItemInfo>())
                {
                    if (Input.GetButtonDown("Drop"))
                    {
                        Drop();
                    }

                    if(Input.GetButtonDown("Fire3"))
                    {
                        Throw();
                    }
                }
            }
        }

        void Drop()
        {
            NetworkViewID itId = equippedItem.GetComponent<NetworkView>().viewID;
            PlayerController controller = myPlayer.GetComponent<PlayerController>();

            Transform launchTransform = Camera.main.transform;
            Vector3 launchPos = launchTransform.TransformPoint(0, 0, 1f);
            Quaternion launchRot = Quaternion.Euler(launchTransform.eulerAngles.x, launchTransform.eulerAngles.y, transform.eulerAngles.z);

            netView.RPC("TellWhatIDropped", RPCMode.OthersBuffered, itId, launchPos, launchRot, equippedItem.GetComponent<ItemInfo>().amount);

            equippedItem.transform.parent = null;
            equippedItem.GetComponent<ItemInfo>().owner = null;
            equippedItem.transform.position = launchPos;
            equippedItem.transform.rotation = launchRot;
            equippedItem.GetComponent<Rigidbody>().isKinematic = false;
            equippedItem.GetComponent<Collider>().isTrigger = false;

            equippedItem = null;

            Transform bone = myAnim.GetBoneTransform(HumanBodyBones.RightHand);

            for (int i = 0; i < bone.childCount; i++)
            {
                if (bone.GetChild(i).tag == "Item")
                {
                    Destroy(bone.GetChild(i).gameObject);
                }
            }

            AddFists();
            ResetController();

            Equip(Items[currentSelected]);

            netView.RPC("TellIDeequiped", RPCMode.OthersBuffered, GetComponent<NetworkView>().viewID);
        }

        void Throw()
        {
            NetworkViewID itId = equippedItem.GetComponent<NetworkView>().viewID;
            PlayerController controller = myPlayer.GetComponent<PlayerController>();

            Transform launchTransform = Camera.main.transform;
            Vector3 launchPos = launchTransform.TransformPoint(0, 0, 1.5f);
            Quaternion launchRot = Quaternion.Euler(launchTransform.eulerAngles.x + 180, launchTransform.eulerAngles.y, transform.eulerAngles.z);

            netView.RPC("TellWhatIThrew", RPCMode.OthersBuffered, itId, Camera.main.transform.forward, launchPos, launchRot, equippedItem.GetComponent<ItemInfo>().amount);

            equippedItem.transform.parent = null;
            equippedItem.GetComponent<ItemInfo>().owner = null;
            equippedItem.transform.position = launchPos;
            equippedItem.transform.rotation = launchRot;
            equippedItem.GetComponent<Rigidbody>().isKinematic = false;
            equippedItem.GetComponent<Collider>().isTrigger = false;
            equippedItem.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * 5000);
            equippedItem.GetComponent<ItemInfo>().isProjectile = true;

            equippedItem = null;

            Transform bone = myAnim.GetBoneTransform(HumanBodyBones.RightHand);

            for (int i = 0; i < bone.childCount; i++)
            {
                if (bone.GetChild(i).tag == "Item")
                {
                    Destroy(bone.GetChild(i).gameObject);
                }
            }

            AddFists();
            ResetController();

            Equip(Items[currentSelected]);

            netView.RPC("TellIDeequiped", RPCMode.OthersBuffered, GetComponent<NetworkView>().viewID);
        }

        void ControlPickingUp()
        {
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out itemHit, 4, itemMask))
            {
                if (itemHit.collider.tag == "Item")
                {
                    if (itemHit.transform.GetComponent<ItemInfo>())
                    {
                        ItemInfo info = itemHit.transform.GetComponent<ItemInfo>();

                        if (info.owner == null && !info.isProjectile)
                        {
                            if (Input.GetButtonDown("Interact"))
                            {
                                if (FindEmptySlot() != -1)
                                {
                                    int slotToSelect = FindEmptySlot();
                                    AddItem(info.item.Id, info.amount);

                                    NetworkViewID netId = info.GetComponent<NetworkView>().viewID;
                                    netView.RPC("TellWhatIPickedUp", RPCMode.AllBuffered, netId);

                                    if (currentSelected != slotToSelect)
                                    {
                                        SelectSlot(slotToSelect);
                                    }
                                    else
                                    {
                                        Equip(Items[slotToSelect]);
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }

        void UpdateSelectorPosition()
        {
            if (currentSelected != -1)
            {
                selector.transform.position = Slots[currentSelected].transform.position;
            }
        }

        public void AddFists()
        {
            Item itemToAdd = db.FetchItemById(0);
            Items[currentSelected] = itemToAdd;
            GameObject itemGO = Slots[currentSelected].transform.GetChild(0).gameObject;
            itemGO.GetComponent<Image>().sprite = itemToAdd.Sprite;
            itemGO.name = itemToAdd.Slug;
            ItemData data = itemGO.GetComponent<ItemData>();
            data.Item = itemToAdd;
            data.SlotID = currentSelected;
            data.Amount = itemToAdd.Amount;
            itemGO.transform.GetChild(0).GetComponent<Text>().text = "";
        }

        public void AddItem(int id)
        {
            Item itemToAdd = db.FetchItemById(id);
            int slotToAdd = -1;

            slotToAdd = FindEmptySlot();

            if (slotToAdd != -1)
            {
                GameObject itemGO = Slots[slotToAdd].transform.GetChild(0).gameObject;
                Items[slotToAdd] = itemToAdd;
                itemGO.GetComponent<Image>().sprite = itemToAdd.Sprite;
                itemGO.name = itemToAdd.Slug;
                ItemData data = itemGO.GetComponent<ItemData>();
                data.Item = itemToAdd;
                data.SlotID = slotToAdd;
                data.Amount = itemToAdd.Amount;

                if (itemToAdd.Stackable)
                {
                    itemGO.GetComponentInChildren<Text>().text = itemToAdd.Amount.ToString();
                }
                else if (itemToAdd.Type == EItemType.Weapons)
                {
                    Weapon weapon = (Weapon)itemToAdd;
                    if (weapon.WeaponType == EWeaponType.Bow)
                    {
                        itemGO.GetComponentInChildren<Text>().text = itemToAdd.Amount.ToString();
                    }
                }
                else
                {
                    itemGO.transform.GetChild(0).GetComponent<Text>().text = "";
                }
            }
        }

        public void AddItem(int id, int amt)
        {
            Item itemToAdd = db.FetchItemById(id);
            int slotToAdd = -1;

            slotToAdd = FindEmptySlot();

            if (slotToAdd != -1)
            {
                GameObject itemGO = Slots[slotToAdd].transform.GetChild(0).gameObject;
                Items[slotToAdd] = itemToAdd;
                itemGO.GetComponent<Image>().sprite = itemToAdd.Sprite;
                itemGO.name = itemToAdd.Slug;
                ItemData data = itemGO.GetComponent<ItemData>();
                data.Item = itemToAdd;
                data.SlotID = slotToAdd;
                data.Amount = amt;

                if (itemToAdd.Stackable)
                {
                    itemGO.GetComponentInChildren<Text>().text = itemToAdd.Amount.ToString();
                }
                else if(itemToAdd.Type == EItemType.Weapons)
                {
                    Weapon weapon = (Weapon)itemToAdd;
                    if(weapon.WeaponType == EWeaponType.Bow)
                    {
                        itemGO.GetComponentInChildren<Text>().text = amt.ToString();
                    }
                }
                else
                {
                    itemGO.transform.GetChild(0).GetComponent<Text>().text = "";
                }
            }
        }

        int FindEmptySlot()
        {
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].Id == 0 || Items[i].Id == -1)
                {
                    return i;
                }
            }

            return -1;
        }

        public void Equip(Item it)
        {
            PlayerController controller = myPlayer.GetComponent<PlayerController>();
            controller.ResetAllTimers();
            controller.Deployed = false;            

            if (it.Id != 0)
            {
                SpawnItem(it);        
            }
            else
            {
                controller.HeldItem = it;
                Weapon weapon = (Weapon)it;
                controller.MeleeReach = weapon.Reach;
                controller.SwingSpeed = weapon.Speed;
                equippedItem = null;
            }
        }

        void SpawnItem(Item it)
        {
            var spdItem = Network.Instantiate(it.Prefab, transform.position, transform.rotation, 0);
            
            GameObject spawnedItem = (GameObject)spdItem;
            ItemData dat = Slots[currentSelected].transform.GetChild(0).gameObject.GetComponent<ItemData>();

            spawnedItem.GetComponent<ItemInfo>().item = it;
            spawnedItem.GetComponent<ItemInfo>().amount = dat.Amount;

            if (it.Type == EItemType.Weapons)
            {
                Weapon weapon = (Weapon)it;

                if (weapon.WeaponType == EWeaponType.Bow)
                {
                    spawnedItem.transform.SetParent(myAnim.GetBoneTransform(HumanBodyBones.LeftHand));
                    spawnedItem.transform.localPosition = bowEquipPos;
                    spawnedItem.transform.localRotation = Quaternion.Euler(bowEquipRot);
                    GameObject arrow = Instantiate(Resources.Load<GameObject>("Prefabs/Items/Others/Arrow"));
                    arrow.transform.SetParent(myAnim.GetBoneTransform(HumanBodyBones.RightHand));
                    arrow.transform.localPosition = arrowEquipPos;
                    arrow.transform.localRotation = Quaternion.Euler(arrowEquipRot);
                }
                else
                {
                    spawnedItem.transform.SetParent(myAnim.GetBoneTransform(HumanBodyBones.RightHand));
                    spawnedItem.transform.localPosition = equipPos;
                    spawnedItem.transform.localRotation = Quaternion.Euler(equipRot);
                }

                controller.MeleeReach = weapon.Reach;
                controller.SwingSpeed = weapon.Speed;
            }
            else
            {
                if(it.Type == EItemType.Consumables)
                {
                    Consumable consumable = (Consumable)it;
                    GetComponent<Progress>().InteractionTime = consumable.Time;
                }

                spawnedItem.transform.SetParent(myAnim.GetBoneTransform(HumanBodyBones.RightHand));
                spawnedItem.transform.localPosition = equipPos;
                spawnedItem.transform.localRotation = Quaternion.Euler(equipRot);
            }

            equippedItem = spawnedItem;
            controller.ItemVis = spawnedItem;
            controller.HeldItem = it;

            NetworkViewID idToSend = spawnedItem.GetComponent<NetworkView>().viewID;

            netView.RPC("SetupName", RPCMode.AllBuffered, idToSend, it.Slug);
            netView.RPC("TellWhatIEquipped", RPCMode.OthersBuffered, idToSend, Network.player);
        }

        public void Deequip()
        {
            if (equippedItem)
            {
                Item it = equippedItem.GetComponent<ItemInfo>().item;

                Destroy(equippedItem);

                if (it.Type == EItemType.Weapons)
                {
                    Weapon wep = (Weapon)it;

                    if (wep.WeaponType == EWeaponType.Bow)
                    {
                        Transform bone = myAnim.GetBoneTransform(HumanBodyBones.RightHand);

                        for (int i = 0; i < bone.childCount; i++)
                        {
                            if (bone.GetChild(i).tag == "Item")
                            {
                                Destroy(bone.GetChild(i).gameObject);
                            }
                        }
                    }
                }

                netView.RPC("TellIDeequiped", RPCMode.OthersBuffered, GetComponent<NetworkView>().viewID);

                ResetController();
            }
        }

        public void DeductAmmo()
        {
            ItemData dat = Slots[currentSelected].transform.GetChild(0).GetComponent<ItemData>();
            dat.Amount--;
            dat.gameObject.GetComponentInChildren<Text>().text = dat.Amount.ToString();
        }

        public void DropEverything()
        {
            for(int i = 0; i < Slots.Count; i++)
            {
                if(Items[i].Id != 0)
                {
                    SelectSlot(i);
                    Drop();
                }
            }
        }

        public void RemoveItem()
        {
            equippedItem = null;

            Transform bone = myAnim.GetBoneTransform(HumanBodyBones.RightHand);

            for (int i = 0; i < bone.childCount; i++)
            {
                if (bone.GetChild(i).tag == "Item")
                {
                    Destroy(bone.GetChild(i).gameObject);
                }
            }

            AddFists();
            ResetController();

            Equip(Items[currentSelected]);

            netView.RPC("TellIDeequiped", RPCMode.OthersBuffered, GetComponent<NetworkView>().viewID);
        }

        void ResetController()
        {
            PlayerController controller = myPlayer.GetComponent<PlayerController>();

            controller.HeldItem = null;
            controller.MeleeReach = 0f;
            equippedItem = null;
            controller.ResetAllTimers();
            controller.NoWeapon();
        }

        [RPC]
        void TellWhatIDropped(NetworkViewID itId, Vector3 pos, Quaternion rot, int amt)
        {
            Transform droppedItem = NetworkView.Find(itId).transform;
            droppedItem.parent = null;
            droppedItem.GetComponent<ItemInfo>().owner = null;
            droppedItem.GetComponent<ItemInfo>().amount = amt;
            droppedItem.position = pos;
            droppedItem.rotation = rot;
            droppedItem.GetComponent<Rigidbody>().isKinematic = false;
            droppedItem.GetComponent<Collider>().isTrigger = false;
        }

        [RPC]
        void TellWhatIThrew(NetworkViewID itId, Vector3 own, Vector3 pos, Quaternion rot, int amt)
        {
            Transform droppedItem = NetworkView.Find(itId).transform;
            droppedItem.parent = null;
            droppedItem.GetComponent<ItemInfo>().owner = null;
            droppedItem.GetComponent<ItemInfo>().amount = amt;
            droppedItem.position = pos;
            droppedItem.rotation = rot;
            droppedItem.GetComponent<Rigidbody>().isKinematic = false;
            droppedItem.GetComponent<Collider>().isTrigger = false;
            droppedItem.GetComponent<Rigidbody>().AddForce(own * 5000);
            droppedItem.GetComponent<ItemInfo>().isProjectile = true;
        }

        [RPC]
        void TellWhatIEquipped(NetworkViewID itId, NetworkPlayer netPlayer)
        {
            GameObject managers = GameObject.FindGameObjectWithTag("Managers");

            GameObject it = NetworkView.Find(itId).gameObject;
            it.GetComponent<ItemInfo>().item = managers.GetComponent<ItemDatabase>().FetchItemBySlug(it.name);
            Item eqIt = it.GetComponent<ItemInfo>().item;
            
            Transform owner = managers.GetComponent<MultiplayerDatabase>().FetchPlayer(netPlayer).Vis.transform;
            Animator anim = owner.GetComponent<Animator>();

            if (eqIt.Type == EItemType.Weapons)
            {
                Weapon wep = (Weapon)eqIt;

                if (wep.WeaponType == EWeaponType.Bow)
                {
                    it.transform.SetParent(anim.GetBoneTransform(HumanBodyBones.LeftHand));
                    it.transform.localPosition = bowEquipPos;
                    it.transform.localRotation = Quaternion.Euler(bowEquipRot);

                    GameObject arrow = Instantiate(Resources.Load<GameObject>("Prefabs/Items/Others/Arrow"));
                    arrow.transform.SetParent(anim.GetBoneTransform(HumanBodyBones.RightHand));
                    arrow.transform.localPosition = arrowEquipPos;
                    arrow.transform.localRotation = Quaternion.Euler(arrowEquipRot);
                }
                else
                {
                    it.transform.SetParent(anim.GetBoneTransform(HumanBodyBones.RightHand));
                    it.transform.localPosition = equipPos;
                    it.transform.localRotation = Quaternion.Euler(equipRot);
                }
            }
            else
            {
                it.transform.SetParent(anim.GetBoneTransform(HumanBodyBones.RightHand));
                it.transform.localPosition = equipPos;
                it.transform.localRotation = Quaternion.Euler(equipRot);
            }            
        }

        [RPC]
        void TellIDeequiped(NetworkViewID id)
        {
            GameObject target = NetworkView.Find(id).gameObject;
            Animator anim = target.GetComponent<Animator>();
            Transform bone = anim.GetBoneTransform(HumanBodyBones.LeftHand);
            
            for(int i = 0; i < bone.childCount; i++)
            {
                if(bone.GetChild(i).tag == "Item")
                {
                    Destroy(bone.GetChild(i).gameObject);
                }
            }

            bone = anim.GetBoneTransform(HumanBodyBones.RightHand);

            for (int i = 0; i < bone.childCount; i++)
            {
                if (bone.GetChild(i).tag == "Item")
                {
                    Destroy(bone.GetChild(i).gameObject);
                }
            }
        }

        [RPC]
        void TellWhatIPickedUp(NetworkViewID id)
        {
            GameObject target = NetworkView.Find(id).gameObject;

            Destroy(target);
        }

        [RPC]
        void SetupName(NetworkViewID itId, string slug)
        {
            NetworkView.Find(itId).name = slug;
        }

        public void EnableHotbarGfx()
        {
            if (!hotbar.activeSelf)
            {
                hotbar.SetActive(true);
            }
        }

        public void DisableHotbarGfx()
        {
            if (hotbar.activeSelf)
            {
                hotbar.SetActive(false);
            }
        }

        void EnableVis()
        {
            selector.SetActive(true);
        }

        void DisableVis()
        {
            selector.SetActive(false);
        }
    }
}