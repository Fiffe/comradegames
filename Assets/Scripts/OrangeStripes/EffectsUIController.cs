﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace OrangeStripes
{

    public class EffectsUIController : MonoBehaviour
    {
        private NetworkView netView;

        //VARIABLES

        private PlayerController controller;
        private GameObject effectsPanel;
        private GameObject effectPrefab;

        public bool isStunned;
        public float stunTimer;
        public float stunDuration;
        private Image stunDur;
        private GameObject stunVis;

        public bool isSlowed;
        public float slowTimer;
        public float slowDuration;
        private Image slowDur;
        private GameObject slowVis;

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
        }

        void Start()
        {
            if (netView.isMine)
            {
                controller = GetComponent<PlayerController>();
                effectsPanel = GameObject.Find("EffectsPanel");
                effectPrefab = Resources.Load<GameObject>("Prefabs/UI/Effect");
            }
            else
            {
                enabled = false;
            }
        }

        void Update()
        {
            slowTimer = Mathf.Clamp(slowTimer, 0, slowDuration);
            stunTimer = Mathf.Clamp(stunTimer, 0, stunDuration);

            if(isStunned)
            {
                stunTimer -= Time.deltaTime;

                if(stunVis)
                {
                    stunDur.fillAmount = stunTimer / stunDuration;

                    if(stunTimer <= 0)
                    {
                        isStunned = false;
                        Destroy(stunVis);
                    }
                }
                else
                {
                    CreateEffect(0);
                }
            }

            if(isSlowed)
            {
                slowTimer -= Time.deltaTime;

                if(slowVis)
                {
                    slowDur.fillAmount = stunTimer / stunDuration;

                    if (slowTimer <= 0)
                    {
                        isSlowed = false;
                        Destroy(slowVis);
                    }
                }
                else
                {
                    CreateEffect(1);
                }
            }
        }

        void CreateEffect(int id)
        {
            switch (id)
            {
                case 0:
                    stunVis = Instantiate(effectPrefab);
                    stunVis.transform.SetParent(effectsPanel.transform);
                    stunDur = stunVis.transform.GetChild(0).GetComponent<Image>();
                    break;
                case 1:
                    slowVis = Instantiate(effectPrefab);
                    slowVis.transform.SetParent(effectsPanel.transform);
                    slowDur = stunVis.transform.GetChild(0).GetComponent<Image>();
                    break;
            }
        }

        public void StartEffect(int id, float dur)
        {
            switch(id)
            {
                case 0:
                    stunDuration = dur;
                    stunTimer = dur;
                    isStunned = true;
                    break;
                case 1:
                    slowDuration = dur;
                    slowTimer = dur;
                    isSlowed = true;
                    break;
            }
        }
    }
}