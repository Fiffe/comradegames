﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class PlayerController : MonoBehaviour
    {
        private NetworkView netView;
        private bool initialSetup = false;

        //VARIABLES

        private Transform myTransform;

        //MOVEMENT

        private float movementSpeed = 5f;
        private float movementMultiplier = 1;

        private float baseMovementSpeed = 5f;
        private float baseJumpSpeed = 10;
        private float baseFallSpeed = 100;
        private float baseRunSpeed = 10f;
        private float baseJumpCooldown = 2;
        private float jumpCooldown = 2;
        private float timeSinceJump;

        private bool canJump;
        private bool pressedJump = true;
        private bool canPressJump = true;
        private bool isGrounded;
        private bool canRun;
        private bool isRunning;
        private bool canMove = true;

        private Vector3 move;

        private float sphereCastRange;

        private float spineRotY = 0;
        private float spineMaxY = 10f;
        private float spineMinY = -10f;
        private float neckRotY = 0;
        private float neckMaxY = 25f;
        private float neckMinY = -25f;

        //GROUND DETECTION

        private LayerMask raycastMask;

        private float groundDetectFactor = 1;
        private float fallDetectFactor = 1;

        //ANIMATOR

        private float horizontal;
        private float vertical;
        private int weaponType;
        private float attackType;

        //REFERENCES

        private Animator anim;
        private Animator localAnim;
        private Rigidbody rigidbody;
        private CapsuleCollider collider;
        private Transform cameraTransform;
        private MultiplayerDatabase database;
        private MultiplayerManager mg;
        private ItemDatabase itemDb;
        private PlayerMetabolism metabolism;
        private MouseController mouse;
        private EffectsUIController effectsController;
        private Hotbar hotbar;

        public BoxCollider blockingCollider;

        public Transform spine;
        public Transform neck;

        //HANDS CONTROL

        private GameObject itemVis;
        private Item heldItem;
        private bool holdingItem;

            //MELEE

        private LayerMask meleeMask;

        private float deployTime = 1.75f;
        private float deployTimer = 0;
        private bool deployed = false;

        private bool canSwing;
        private bool canPressSwing = true;
        private bool pressedSwing;
        private bool swinging;

        private float meleeReach;

        private float swingSpeed;
        private float swingTimer = 0;

        private bool canPush;
        private bool canPressPush = true;
        private bool pressedPush;
        private bool pushing;

        private float pushReach = 2;

        private float basePushCooldown = 2;
        private float pushCooldown = 2;
        private float pushTimer = 0;

        private bool canBlock;
        private bool canPressBlock = true;
        private bool pressedBlock;
        private bool blocking;
        private bool balancing;
        private bool blocked;

        private float baseBlockCooldown = 0.75f;
        private float blockCooldown = 0.75f;
        private float blockTimer = 0;

        private float baseBalanceRegain = 0.5f;
        private float balanceRegain = 0.5f;

        private bool gotHit;
        private bool isAlive = true;

            //INTERRUPTION

        private bool interrupted;
        private bool triggerInterrupt;

        private float baseInterruptionDuration = 1.5f;
        private float interruptionDuration = 1.5f;

            //BOWING

        private GameObject arrowPrefab;
        private float drawTimer = 0;
        private float drawTime = 1f;

        //INTERACTING

        private Progress progressBar;

        private bool interacting = false;
        private bool canInteract = true;

        //EFFECTS

        private bool isSlowed;
        private float slowMultiplier = 1;
        private float baseSlowMultiplier = 1;
        private float slowTimer = 0;
        private float slowDuration = 1;

        private bool isStunned;
        private float stunTimer = 0;
        private float stunDuration;

        //SPINE ROTATION

        public Quaternion spineRot;
        public Quaternion neckRot;

        //PROPERTIES

        public float BaseMovementSpeed
        {
            get
            {
                return baseMovementSpeed;
            }
            set
            {
                baseMovementSpeed = value;
            }
        }

        public float BaseJumpSpeed
        {
            get
            {
                return baseJumpSpeed;
            }
            set
            {
                baseJumpSpeed = value;
            }
        }

        public float BaseFallSpeed
        {
            get
            {
                return baseFallSpeed;
            }
            set
            {
                baseFallSpeed = value;
            }
        }

        public bool IsGrounded
        {
            get
            {
                return isGrounded;
            }
            set
            {
                isGrounded = value;
            }
        }

        public float BaseRunSpeed
        {
            get
            {
                return baseRunSpeed;
            }
            set
            {
                baseRunSpeed = value;
            }
        }

        public bool IsRunning
        {
            get
            {
                return isRunning;
            }
            set
            {
                isRunning = value;
            }
        }

        public bool CanMove
        {
            get
            {
                return canMove;
            }
            set
            {
                canMove = value;
            }
        }

        public float JumpCooldown
        {
            get
            {
                return jumpCooldown;
            }
            set
            {
                jumpCooldown = value;
            }
        }

        public Transform CameraTransform
        {
            get
            {
                return cameraTransform;
            }
            set
            {
                cameraTransform = value;
            }
        }

        public Animator LocalAnim
        {
            get
            {
                return localAnim;
            }
        }

        public MouseController Mouse
        {
            get
            {
                return mouse;
            }
            set
            {
                mouse = value;
            }
        }

        public float MovementMultiplier
        {
            get
            {
                return movementMultiplier;
            }
            set
            {
                movementMultiplier = value;
            }
        }

        public GameObject ItemVis
        {
            get
            {
                return itemVis;
            }
            set
            {
                itemVis = value;
            }
        }

        public int WeaponType
        {
            get
            {
                return weaponType;
            }
            set
            {
                weaponType = value;
            }
        }

        public float AttackType
        {
            get
            {
                return attackType;
            }
        }

        public Item HeldItem
        {
            get
            {
                return heldItem;
            }
            set
            {
                heldItem = value;
            }
        }

        public bool HoldingItem
        {
            get
            {
                return holdingItem;
            }
            set
            {
                holdingItem = value;
            }
        }

        public float SwingSpeed
        {
            get
            {
                return swingSpeed;
            }
            set
            {
                swingSpeed = value;
            }
        }

        public bool Swinging
        {
            get
            {
                return swinging;
            }
            set
            {
                swinging = value;
            }
        }

        public float MeleeReach
        {
            get
            {
                return meleeReach;
            }
            set
            {
                meleeReach = value;
            }
        }

        public bool Pushing
        {
            get
            {
                return pushing;
            }
            set
            {
                pushing = value;
            }
        }

        public bool Blocking
        {
            get
            {
                return blocking;
            }
            set
            {
                blocking = value;
            }
        }

        public bool Blocked
        {
            get
            {
                return blocked;
            }
            set
            {
                blocked = value;
            }
        }

        public float BlockCooldown
        {
            get
            {
                return blockCooldown;
            }
            set
            {
                blockCooldown = value;
            }
        }

        public bool Balancing
        {
            get
            {
                return balancing;
            }
            set
            {
                balancing = value;
            }
        }

        public float BalanceRegain
        {
            get
            {
                return balanceRegain;
            }
            set
            {
                balanceRegain = value;
            }
        }

        public bool Deployed
        {
            get
            {
                return deployed;
            }
            set
            {
                deployed = value;
            }
        }

        public bool GotHit
        {
            get
            {
                return gotHit;
            }
            set
            {
                gotHit = value;
            }
        }

        public bool IsAlive
        {
            get
            {
                return isAlive;
            }
            set
            {
                isAlive = value;
            }
        }

        public bool Interrupted
        {
            get
            {
                return interrupted;
            }
            set
            {
                interrupted = value;
            }
        }

        public float InterruptionDuration
        {
            get
            {
                return interruptionDuration;
            }
            set
            {
                interruptionDuration = value;
            }
        }

        public bool Interacting
        {
            get
            {
                return interacting;
            }
            set
            {
                interacting = value;
            }
        }

        public float Horizontal
        {
            get
            {
                return horizontal;
            }
        }

        public float Vertical
        {
            get
            {
                return vertical;
            }
        }

        public bool IsStunned
        {
            get
            {
                return isStunned;
            }
        }

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
            myTransform = transform;
            anim = GetComponent<Animator>();
            rigidbody = GetComponent<Rigidbody>();
            database = GameObject.FindGameObjectWithTag("Managers").GetComponent<MultiplayerDatabase>();
            itemDb = GameObject.FindGameObjectWithTag("Managers").GetComponent<ItemDatabase>();
            arrowPrefab = Resources.Load<GameObject>("Prefabs/Items/Others/Arrow");
        }

        void InitialSetup()
        {
            if(mg)
            {
                if (mg.MapLoaded && !initialSetup)
                {
                    cameraTransform = Camera.main.transform;
                    collider = GetComponent<CapsuleCollider>();
                    sphereCastRange = myTransform.localScale.y * groundDetectFactor;
                    raycastMask = ~(1 << LayerMask.NameToLayer("Player") | 1 << LayerMask.NameToLayer("Ignore Raycast"));
                    meleeMask = ~(1 << LayerMask.NameToLayer("Default") | 1 << LayerMask.NameToLayer("Ignore Raycast") | 1 << LayerMask.NameToLayer("MyRagdolls") | 1 << LayerMask.NameToLayer("Player") | 1 << LayerMask.NameToLayer("Items"));
                    localAnim = GetComponent<PlayerSetup>().localMesh.GetComponent<Animator>();
                    mouse = GetComponentInChildren<MouseController>();
                    progressBar = GetComponent<Progress>();
                    metabolism = GetComponent<PlayerMetabolism>();
                    effectsController = GetComponent<EffectsUIController>();
                    hotbar = GetComponent<Hotbar>();
                    initialSetup = true;
                }
            }
        }

        void Start()
        {
            if (netView.isMine)
            {
                InitialSetup();
            }
            else
            {
                enabled = false;
            }
        }

        void LateUpdate()
        {
            if (isAlive)
            {
                spineRot = Quaternion.Euler(new Vector3(spine.localRotation.x, spineRotY, spine.localRotation.z));
                neckRot = Quaternion.Euler(new Vector3(spine.localRotation.x, neckRotY, spine.localRotation.z));
                spine.localRotation = spineRot;
                neck.localRotation = neckRot;
            }
        }

        void Update()
        {
            if(!mg && netView.isMine)
            {
                if(GameManager.instance.MultiplayerManager)
                {
                    mg = GameManager.instance.MultiplayerManager;
                }
            }

            if(!initialSetup && netView.isMine)
            {
                InitialSetup();
            }

            isGrounded = CheckForGround();

            if (isAlive)
            {
                if(progressBar.done)
                {
                    if(heldItem.Type == EItemType.Consumables)
                    {
                        Consumable cons = (Consumable)heldItem;

                        metabolism.Heal(cons.Heal);
                    }
                    hotbar.RemoveItem();
                    interacting = false;
                    progressBar.isInteracting = false;
                    progressBar.done = false;
                }

                if(blocking)
                {
                    blockingCollider.enabled = true;
                    netView.RPC("TellImBlocking", RPCMode.OthersBuffered);
                }
                else
                {
                    blockingCollider.enabled = false;
                    netView.RPC("TellImNotBlocking", RPCMode.OthersBuffered);
                }

                if(interacting)
                {
                    progressBar.isInteracting = true;
                    canMove = false;
                }
                else
                {
                    progressBar.isInteracting = false;
                }

                if(isStunned)
                {
                    canMove = false;
                    stunTimer += Time.deltaTime;

                    if(stunTimer >= stunDuration)
                    {
                        stunTimer = 0;
                        isStunned = false;
                    }
                }

                if (gotHit)
                {
                    anim.SetTrigger("GetHit");
                    triggerInterrupt = true;
                    gotHit = false;
                }

                if (blocked)
                {
                    anim.SetTrigger("Blocked");
                    blocked = false;
                }

                if (heldItem != null)
                {
                    if (!deployed)
                    {
                        deployTimer += Time.deltaTime;

                        if (deployTimer >= deployTime)
                        {
                            Deploy();
                            deployed = true;
                        }
                    }

                    if (heldItem.Type == EItemType.Weapons)
                    {
                        holdingItem = true;
                        Weapon wep = (Weapon)heldItem;

                        switch (wep.WeaponType)
                        {
                            case EWeaponType.Fists:
                                weaponType = 0;
                                break;
                            case EWeaponType.Bow:
                                weaponType = 1;
                                break;
                            case EWeaponType.OneHanded:
                                weaponType = 2;
                                break;
                        }

                        WeaponCanceling();

                        if (balancing || interrupted || interacting)
                        {
                            canPush = false;
                            canSwing = false;
                            canJump = false;
                            canBlock = false;
                            swinging = false;
                            blocking = false;
                            pushing = false;
                        }

                        if(Cursor.lockState != CursorLockMode.Locked)
                        {
                            canPush = false;
                            canSwing = false;
                            canJump = false;
                            canBlock = false;
                        }

                        if (weaponType != 1)
                        {
                            if (swinging)
                            {
                                canPush = false;
                                canBlock = false;
                            }

                            if (pushing)
                            {
                                canSwing = false;
                                canBlock = false;
                            }

                            if (blocking)
                            {
                                canSwing = false;
                                canPush = false;
                            }

                            if (!swinging && !pushing && !blocking && !balancing && !interrupted && !interacting && Cursor.lockState == CursorLockMode.Locked)
                            {
                                canSwing = true;
                                canPush = true;
                                canBlock = true;
                                canJump = true;
                            }
                        }
                        else
                        {
                            canPush = false;
                            canBlock = false;

                            if (!swinging && !balancing && !interrupted && !interacting && Cursor.lockState == CursorLockMode.Locked)
                            {
                                canSwing = true;
                                canJump = true;
                            }
                        }
                    }
                    else
                    {
                        weaponType = -1;
                    }

                    if (heldItem.Type == EItemType.Consumables)
                    {
                        canInteract = true;
                    }
                    else
                    {
                        interacting = false;
                        canInteract = false;
                    }
                }
                else
                {
                    weaponType = -1;
                    swinging = false;
                    holdingItem = false;
                    canSwing = false;
                    pushing = false;
                    canPush = false;
                    canBlock = false;
                    blocking = false;
                    canInteract = false;
                }

                if (canBlock)
                {
                    ControlBlocking();
                }

                if(canInteract)
                {
                    ControlInteracting();
                }

                if (canSwing)
                {
                    if (weaponType == 1)
                    {
                        ControlBow();
                    }
                    else
                    {
                        ControlSwing();
                    }
                }

                if (canPush)
                {
                    ControlPush();
                }

                spineRotY = Mathf.Clamp(cameraTransform.GetComponent<MouseController>().MouseY, spineMinY, spineMaxY);
                neckRotY = Mathf.Clamp(cameraTransform.GetComponent<MouseController>().MouseY, neckMinY, neckMaxY);

                //CONTROL INPUTS WITH COOLDOWNS

                if (pressedJump)
                {
                    StartCoroutine(EnableJumping());
                    pressedJump = false;
                }

                if (pressedSwing)
                {
                    swingTimer += Time.deltaTime;

                    if (swingTimer >= swingSpeed)
                    {
                        EnableSwinging();
                        pressedSwing = false;
                    }                    
                }

                if (pressedPush)
                {
                    pushTimer += Time.deltaTime;

                    if (pushTimer >= pushCooldown)
                    {
                        EnablePushing();
                        pressedPush = false;
                    }
                }

                if (pressedBlock)
                {
                    blockTimer += Time.deltaTime;

                    if (blockTimer >= blockCooldown)
                    {
                        EnableBlocking();
                        pressedBlock = false;
                    }
                }

                //CONTROL ABILITIES WITH STATEMENTS

                if (triggerInterrupt)
                {
                    StartCoroutine(StartInterruption());
                    triggerInterrupt = false;
                }

                if (isGrounded)
                {
                    timeSinceJump = 0;
                }

                UpdateAnimator();
                ControlRunning();
            }
        }

        void FixedUpdate()
        {
            ConvertInput();

            if (!isGrounded)
            {
                Airborne();
            }
            else
            {
                Ground();

                if (canJump)
                {
                    ControlJump();
                }
            }

            Move();

            if (timeSinceJump > 0.2f || !isGrounded)
            {
                rigidbody.velocity += (Vector3.up * (-baseFallSpeed + (timeSinceJump / 10)) * Time.deltaTime);
            }
        }

        void ConvertInput()
        {
            if (canMove)
            {
                horizontal = Input.GetAxis("Horizontal");
                vertical = Input.GetAxis("Vertical");

                if (isRunning)
                {
                    movementMultiplier = 2;
                }
                else
                {
                    movementMultiplier = 1;
                }
            }
            else
            {
                horizontal = Mathf.Lerp(horizontal, 0, 2f);
                vertical = Mathf.Lerp(vertical, 0, 2f);
                movementMultiplier = 1;
            }
        }

        void Move()
        {
            if(move != Vector3.zero)
            {
                rigidbody.MovePosition(rigidbody.position + move * Time.deltaTime);
            }
        }

        void Ground()
        {
            if (!canJump && !interrupted && !balancing)
            {
                canJump = true;
            }

            Vector3 movHorizontal = transform.right * horizontal;
            Vector3 movVertical = transform.forward * vertical;

            move = (movHorizontal + movVertical).normalized * movementSpeed; 
        }

        void ControlJump()
        {
            if (Input.GetButtonDown("Jump") && canPressJump && metabolism.CurrentStamina >= 12)
            {
                rigidbody.velocity = new Vector3(move.x, Mathf.Sqrt(2 * baseJumpSpeed * 10), move.z);

                pressedJump = true;
                canPressJump = false;
                metabolism.UseStamina(12);
            }
        }

        void ControlSwing()
        {
            if(Input.GetButtonDown("Fire1") && canPressSwing)
            {
                swinging = true;
                pressedSwing = true;
                canPressSwing = false;
                metabolism.UseStamina(10);

                int random = Random.Range(0, 4);
                attackType = random;

                RaycastHit hit;
                if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, meleeReach, meleeMask))
                {
                    if(hit.transform.gameObject.layer == 10)
                    {
                        TellMyHit(hit.transform.gameObject, (Weapon)heldItem);                      
                    }
                    else if(hit.transform.gameObject.tag == "Resource")
                    {
                    }
                    else
                    {
                        if (hit.collider.gameObject.tag == "Block")
                        {
                            gotHit = true;
                            ChangeStun(.6f);
                            TellMyAttempt(hit.transform.gameObject);
                        }
                    }
                }
            }
        }

        void ControlBow()
        {
            if (itemVis.GetComponent<ItemInfo>().amount > 0)
            {
                if (Input.GetButton("Fire1") && canPressSwing)
                {
                    swinging = true;
                    drawTimer += Time.deltaTime;
                }
                else if (Input.GetButtonUp("Fire1") && drawTimer < drawTime)
                {
                    drawTimer = 0;
                    swinging = false;
                    pressedSwing = true;
                    canPressSwing = false;
                }
                else if (Input.GetButtonUp("Fire1") && drawTimer >= drawTime)
                {
                    drawTimer = 0;
                    swinging = false;
                    pressedSwing = true;
                    canPressSwing = false;
                    ShootArrow();
                }

                if (Input.GetButton("Fire2") && swinging)
                {
                    drawTimer = 0;
                    swinging = false;
                    pressedSwing = true;
                    canPressSwing = false;
                }
            }
            else
            {
                drawTimer = 0;
                swinging = false;
                canPressSwing = false;
            }
        }

        void ControlPush()
        {
            if(Input.GetButtonDown("Push") && canPressPush)
            {
                pushing = true;
                pressedPush = true;
                canPressPush = false;
                metabolism.UseStamina(7);

                RaycastHit hit;
                if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 3, meleeMask))
                {
                    if (hit.transform.gameObject.layer == 10)
                    {
                        TellMyPush(hit.transform.root.gameObject);
                    }
                    else if (hit.transform.gameObject.tag == "Resource")
                    {
                        Debug.Log("Hit resource " + hit.transform.name);
                    }
                    else
                    {
                        if (hit.collider.gameObject.tag == "Block")
                        {
                            TellMyPush(hit.transform.root.gameObject);
                        }
                        Debug.Log("Hit other " + hit.collider.name);
                    }
                }
            }
        }

        void ControlBlocking()
        {
            if (Input.GetButton("Block") && canPressBlock && !interrupted)
            {
                blocking = true;
                metabolism.UseStamina(0);
            }
            else if(Input.GetButtonUp("Block"))
            {
                blocking = false;
                pressedBlock = true;
                canPressBlock = false;
            }
        }

        void WeaponCanceling()
        {
            if (Input.GetButtonDown("Fire2") && blocking)
            {
                blocking = false;
                pressedBlock = true;
                canPressBlock = false;
                netView.RPC("TellImNotBlocking", RPCMode.OthersBuffered);
            }

            if(Input.GetButtonDown("Fire2") && swinging)
            {
                drawTimer = 0;
                swinging = false;
                pressedSwing = true;
                canPressSwing = false;
            }
        }

        void ControlInteracting()
        {
            if (Input.GetButton("Fire1") && canInteract)
            {
                interacting = true;
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                interacting = false;
            }
            else if(Input.GetButtonDown("Fire2"))
            {
                interacting = false;
            }
        }

        void ControlRunning()
        {
            if(Input.GetButton("Sprint") && isGrounded && metabolism.CurrentStamina > 0)
            {
                movementSpeed = baseRunSpeed * slowMultiplier;
                metabolism.UseStamina(.1f);
                isRunning = true;
            }
            else
            {
                movementSpeed = baseMovementSpeed * slowMultiplier;
                isRunning = false;
            }
        }

        void ControlEffects()
        {
            if(isSlowed)
            {
                slowTimer += Time.deltaTime;

                if(slowTimer >= slowDuration)
                {
                    slowMultiplier = baseSlowMultiplier;
                    
                    isSlowed = false;
                    slowTimer = 0;
                }
            }
        }

        public void ChangeSlow(float power, float time)
        {
            slowMultiplier = power;
            slowDuration = time;
            slowTimer = 0;
            isSlowed = true;
            effectsController.StartEffect(1, time);
        }

        public void ChangeStun(float time)
        {
            stunDuration = time;
            stunTimer = 0;
            isStunned = true;
            effectsController.StartEffect(0, time);
        }

        void Airborne()
        {
            canPressJump = false;
            pressedJump = true;
            canJump = false;

            timeSinceJump += Time.deltaTime;
        }

        void UpdateAnimator()
        {
            anim.SetFloat("Horizontal", horizontal * movementMultiplier, .2f, Time.deltaTime);
            anim.SetFloat("Vertical", vertical * movementMultiplier, .2f, Time.deltaTime);
            anim.SetBool("Swing", swinging);
            anim.SetBool("Push", pushing);
            anim.SetBool("Block", blocking);
            anim.SetInteger("WeaponType", weaponType);
            anim.SetFloat("AttackType", attackType);
            anim.SetBool("Use", interacting);

            if(weaponType == 1)
            {
                itemVis.GetComponent<Animator>().SetBool("Pulling", swinging);
                itemVis.GetComponent<ItemInfo>().UpdateAnimator("Pulling", swinging);
            }
        }

        bool CheckForGround()
        {
            Ray raySphere = new Ray(myTransform.TransformPoint(0, transform.localScale.y, 0), -myTransform.up);
            return Physics.SphereCast(raySphere, 0.5f, sphereCastRange, raycastMask);
        }

        public void EnableMouse()
        {
            if(!mouse.enabled)
            {
                mouse.enabled = true;
            }
        }

        public void DisableMouse()
        {
            if(mouse.enabled)
            {
                mouse.enabled = false;
            }
        }

        void Deploy()
        {
            canPressSwing = true;
            canPressBlock = true;
            canPressPush = true;
            deployTimer = 0;
        }

        public void ResetAllTimers()
        {
            swingTimer = 0;
            pushTimer = 0;
            blockTimer = 0;
            deployTimer = 0;
        }

        IEnumerator EnableJumping()
        {
            yield return new WaitForSeconds(jumpCooldown);
            canPressJump = true;
        }

        void EnableSwinging()
        {
            canPressSwing = true;
            swingTimer = 0;
        }

        void EnablePushing()
        {
            canPressPush = true;
            pushTimer = 0;
        }

        IEnumerator StartInterruption()
        {
            yield return new WaitForSeconds(interruptionDuration);
            interrupted = false;
        }

        void EnableBlocking()
        {
            canPressBlock = true;
            blockTimer = 0;
        }

        IEnumerator StartBlocked()
        {
            yield return new WaitForSeconds(balanceRegain);
            balancing = false;
        }

        public void DisableSwing()
        {
            swinging = false;
        }

        public void DisablePush()
        {
            pushing = false;
        }

        public void NoWeapon()
        {
            canPressBlock = false;
            canPressPush = false;
            canPressSwing = false;
            swinging = false;
            holdingItem = false;
            canSwing = false;
            pushing = false;
            canPush = false;
            canBlock = false;
            blocking = false;
        }

        void ShootArrow()
        {
            Transform launchTransform = Camera.main.transform;
            Vector3 launchPos = launchTransform.TransformPoint(0, 0, 1.5f);
            Quaternion launchRot = Quaternion.Euler(launchTransform.eulerAngles.x + 180, launchTransform.eulerAngles.y, myTransform.eulerAngles.z);
            string myName = database.FetchPlayer(Network.player).PlayerName;
            Weapon wep = (Weapon)heldItem;
            int dmg = wep.Damage;
            itemVis.GetComponent<ItemInfo>().amount--;
            hotbar.DeductAmmo();

            netView.RPC("SpawnArrow", RPCMode.All, launchPos, launchRot, myName, dmg);
        }

        [RPC]
        void SpawnArrow(Vector3 pos, Quaternion rot, string owner, int dmg)
        {
            GameObject arw = Instantiate(arrowPrefab, pos, rot) as GameObject;
            arw.AddComponent<Projectile>();
            arw.GetComponent<Projectile>().Owner = owner;
            arw.GetComponent<Projectile>().damage = dmg;
            arw.GetComponent<Collider>().isTrigger = true;            
        }

        void TellMyHit(GameObject target, Weapon it)
        {
            target.GetComponentInParent<PlayerMetabolism>().Damage(it.Damage, it.DamageType, target.name);
            target.GetComponentInParent<PlayerMetabolism>().GetPushed(500, transform.position);
        }

        void TellMyAttempt(GameObject target)
        {
            target.GetComponentInParent<PlayerMetabolism>().BlockHit();
        }

        void TellMyPush(GameObject target)
        {
            target.GetComponentInParent<PlayerMetabolism>().GetPushed(1000, transform.position);
        }

        [RPC]
        void TellImBlocking()
        {
            blockingCollider.enabled = true;
        }

        [RPC]
        void TellImNotBlocking()
        {
            blockingCollider.enabled = false;
        }
    }
}