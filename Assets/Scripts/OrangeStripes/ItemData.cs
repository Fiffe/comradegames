﻿using UnityEngine;

namespace OrangeStripes
{
    public class ItemData : MonoBehaviour
    {
        //VARIABLES

        private Item item;
        private int amount;
        private int slotId;

        //PROPERTIES

        public Item Item
        {
            get
            {
                return item;
            }
            set
            {
                item = value;
            }
        }

        public int SlotID
        {
            get
            {
                return slotId;
            }
            set
            {
                slotId = value;
            }
        }

        public int Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }
    }
}