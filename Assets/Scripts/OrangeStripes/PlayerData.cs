﻿using UnityEngine;

namespace OrangeStripes
{
    [System.Serializable]
    public class PlayerData
    {
        //VARIABLES
        
        private int networkPlayer;
        private string playerName;
        private int score;
        private string killerName;
        private GameObject vis;
        private NetworkViewID netViewId;

        //PROPERTIES
        
        public int NetworkPlayer
        {
            get
            {
                return networkPlayer;
            }
            set
            {
                networkPlayer = value;
            }
        }

        public string PlayerName
        {
            get
            {
                return playerName;
            }
            set
            {
                playerName = value;
            }
        }

        public int Score
        {
            get
            {
                return score;
            }
            set
            {
                score = value;
            }
        }

        public string KillerName
        {
            get
            {
                return killerName;
            }
            set
            {
                killerName = value;
            }
        }

        public GameObject Vis
        {
            get
            {
                return vis;
            }
            set
            {
                vis = value;
            }
        }

        public NetworkViewID NetViewId
        {
            get
            {
                return netViewId;
            }
            set
            {
                netViewId = value;
            }
        }

        //FUNCTIONS

        public PlayerData Constructor()
        {
            PlayerData capture = new PlayerData();

            capture.NetworkPlayer = networkPlayer;
            capture.PlayerName = playerName;
            capture.Score = score;
            capture.KillerName = killerName;

            return capture;
        }
    }
}