﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class PlayerMovementSync : MonoBehaviour
    {
        private NetworkView netView;

        //VARIABLES

        private Transform myTransform;

        private Vector3 lastPos;
        private Quaternion lastRot;

        private float lastHorizontal;
        private float lastVertical;

        private PlayerController controller;
        private Animator anim;
        private float moveMultiplier;
        private bool isRunning;
        private bool swinging;
        private bool pushing;
        private bool gotHit;
        private bool blocking;
        private bool blocked;
        private int weaponType;
        private float attackType;
        private bool interacting;

        //PROPERTIES

        public bool Swinging
        {
            get
            {
                return swinging;
            }
            set
            {
                swinging = value;
            }
        }

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
            anim = GetComponent<Animator>();
        }

        void Start()
        {
            if(netView.isMine)
            {
                myTransform = transform;

                controller = GetComponent<PlayerController>();

                netView.RPC("UpdatePosition", RPCMode.OthersBuffered, myTransform.position);
                netView.RPC("UpdateRotation", RPCMode.OthersBuffered, myTransform.rotation);
            }
            else
            {
                enabled = false;
            }
        }

        void Update()
        {
            if (Network.peerType != NetworkPeerType.Disconnected)
            {
                UpdateValues();

                if (Vector3.Distance(myTransform.position, lastPos) > 0.05f)
                {
                    lastPos = myTransform.position;
                    netView.RPC("UpdatePosition", RPCMode.OthersBuffered, myTransform.position);
                }

                if (Quaternion.Angle(myTransform.rotation, lastRot) > 1)
                {
                    lastRot = myTransform.rotation;
                    netView.RPC("UpdateRotation", RPCMode.OthersBuffered, myTransform.rotation);
                }

                if(gotHit)
                {
                    netView.RPC("TellHit", RPCMode.Others);
                }

                if(blocked)
                {
                    netView.RPC("TellBlocked", RPCMode.Others);
                }

                netView.RPC("UpdateMyAnimator", RPCMode.Others, Input.GetAxis("Horizontal") * moveMultiplier, Input.GetAxis("Vertical") * moveMultiplier, swinging, pushing, blocking, weaponType, attackType, interacting);
            }
        }

        void UpdateValues()
        {
            moveMultiplier = controller.MovementMultiplier;
            isRunning = controller.IsRunning;
            swinging = controller.Swinging;
            pushing = controller.Pushing;
            gotHit = controller.GotHit;
            blocking = controller.Blocking;
            blocked = controller.Blocked;
            weaponType = controller.WeaponType;
            attackType = controller.AttackType;
            interacting = controller.Interacting;
        }

        [RPC]
        void UpdatePosition(Vector3 pos)
        {
            transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * 12);
        }

        [RPC]
        void UpdateRotation(Quaternion rot)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * 10);
        }

        [RPC]
        void UpdateMyAnimator(float horizontal, float vertical, bool sw, bool ps, bool bl, int wt, float at, bool use)
        {
            anim.SetFloat("Horizontal", horizontal, .2f, Time.deltaTime);
            anim.SetFloat("Vertical", vertical, .2f, Time.deltaTime);
            anim.SetBool("Swing", sw);
            anim.SetBool("Push", ps);
            anim.SetBool("Block", bl);
            anim.SetInteger("WeaponType", wt);
            anim.SetFloat("AttackType", at);
            anim.SetBool("Use", use);
        }

        [RPC]
        void TellHit()
        {
            anim.SetTrigger("GetHit");
        }

        [RPC]
        void TellBlocked()
        {
            anim.SetTrigger("Blocked");
        }

        [RPC]
        void TellWounded(bool wd)
        {
            anim.SetBool("Wounded", wd);
        }
    }
}