﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace OrangeStripes
{
    public class Scoreboard : MonoBehaviour
    {
        private NetworkView netView;
        private bool initialSetup;

        //VARIABLES

        private GameObject scoreSlotPrefab;
        private GameObject scoreCanvas;
        private Transform parentPanel;

        private List<ScoreSlot> ScoreSlots = new List<ScoreSlot>();
        private List<GameObject> Slots = new List<GameObject>();

        private PlayerData myData;
        private MultiplayerDatabase plDb;
        private MultiplayerManager mg;
        private GameManager gm;

        public bool debug;

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
        }

        void InitialSetup()
        {
            if(mg)
            {
                if(mg.GameStarted && !initialSetup)
                {
                    if (Network.peerType == NetworkPeerType.Client)
                    {
                        scoreSlotPrefab = Resources.Load<GameObject>("Prefabs/UI/ScoreSlot");
                        scoreCanvas = GameObject.Find("ScoreCanvas");
                        parentPanel = scoreCanvas.transform.GetChild(0).transform;
                        plDb = mg.GetComponent<MultiplayerDatabase>();
                        scoreCanvas.SetActive(false);
                        myData = plDb.FetchPlayer(Network.player);

                        CreateScoreboard();

                        initialSetup = true;
                    }
                    else
                    {
                        enabled = false;
                    }
                }
            }
            else
            {
                if (gm)
                {
                    mg = gm.MultiplayerManager;
                }
                else
                {
                    if(GameManager.instance)
                    {
                        gm = GameManager.instance;
                    }
                }
            }
        }

        void Start()
        {
            if (GameManager.instance)
            {
                gm = GameManager.instance;
                if (gm)
                {
                    mg = gm.MultiplayerManager;
                }
            }

            InitialSetup();
        }

        void Update()
        {
            if (!initialSetup && netView.isMine)
            {
                InitialSetup();
            }

            if(initialSetup && netView.isMine)
            {
                if(Input.GetButton("Scoreboard"))
                {
                    scoreCanvas.SetActive(true);
                }
                else
                {
                    if(scoreCanvas.activeSelf)
                    {
                        scoreCanvas.SetActive(false);
                    }
                }

                if(debug)
                {
                    netView.RPC("UpdatePlayer", RPCMode.AllBuffered, Network.player);
                    debug = false;
                }
            }
        }

        void CreateScoreboard()
        {
            for(int i = 0; i < plDb.Players.Count; i++)
            {
                PlayerData _data = plDb.Players[i];
                GameObject slot = Instantiate(scoreSlotPrefab);
                slot.GetComponent<ScoreSlot>().data = _data;
                slot.name = _data.PlayerName;
                slot.GetComponent<Text>().text = _data.PlayerName + " : " + _data.Score + " : " + "Alive" + " : " + " - ";
                slot.transform.SetParent(parentPanel, false);
                ScoreSlots.Add(slot.GetComponent<ScoreSlot>());
                Slots.Add(slot);
            }
        }

        ScoreSlot FindPlayer(NetworkPlayer np)
        {
            for (int i = 0; i < ScoreSlots.Count; i++)
            {
                if (ScoreSlots[i].data.NetworkPlayer == int.Parse(np.ToString()))
                {
                    return ScoreSlots[i];
                }
            }

            return null;
        }

        [RPC]
        void UpdatePlayer(NetworkPlayer np)
        {
            PlayerData _data = plDb.FetchPlayer(np);
            _data.Score++;
            ScoreSlot hisSlot = FindPlayer(np);
            hisSlot.data = _data;

            UpdateSlot(np);
        }

        void UpdateSlot(NetworkPlayer np)
        {
            ScoreSlot hisSlot = FindPlayer(np);

            for (int i = 0; i < Slots.Count; i++)
            {
                if (Slots[i].GetComponent<ScoreSlot>().data.NetworkPlayer == hisSlot.data.NetworkPlayer)
                {
                    Slots[i].GetComponent<Text>().text = hisSlot.data.PlayerName + " : " + hisSlot.data.Score + " : " + "Alive" + " : " + " - ";
                }
            }
        }
    }
}