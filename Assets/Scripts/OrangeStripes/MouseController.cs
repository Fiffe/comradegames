﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class MouseController : MonoBehaviour
    {
        //VARIABLES

        private float sensitivity = 2;

        private float mouseX;
        private float mouseY;

        private float maxY = 65;
        private float minY = -45;

        private Quaternion newRot;

        private Transform playerTransform;
        private Transform cameraTransform;
        
        //PROPERTIES

        public float Sensitivity
        {
            get
            {
                return sensitivity;
            }
            set
            {
                sensitivity = value;
            }
        }

        public float MouseY
        {
            get
            {
                return mouseY;
            }
        }

        //FUNCTIONS

        void Awake()
        {
            cameraTransform = transform;
            playerTransform = transform.parent;
        }

        void FixedUpdate()
        {
            ConvertInput();
            Rotation();
        }

        void ConvertInput()
        {
            mouseX += Input.GetAxis("Mouse X") * sensitivity;
            mouseY -= Input.GetAxis("Mouse Y") * sensitivity;

            mouseY = Mathf.Clamp(mouseY, minY, maxY);
        }

        void Rotation()
        {
            playerTransform.localRotation = Quaternion.Euler(new Vector3(playerTransform.localRotation.x, mouseX, playerTransform.localRotation.z));
            newRot = Quaternion.Euler(new Vector3(mouseY, mouseX, playerTransform.localRotation.z));
            cameraTransform.rotation = newRot;
        }
    }
}