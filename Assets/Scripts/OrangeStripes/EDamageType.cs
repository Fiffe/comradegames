﻿using System;

namespace OrangeStripes
{
    public enum EDamageType
    {
        Blunt,
        Cut,
        Fire,
        Temperature,
        Fall,
        Bullet
    }
}