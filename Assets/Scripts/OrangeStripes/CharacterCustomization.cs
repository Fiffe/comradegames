﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace OrangeStripes
{
    public class CharacterCustomization : MonoBehaviour
    {
        private static CharacterCustomization instance;

        public Camera previewCamera;
        public Camera iconCamera;

        public Text title;
        public GameObject slot;
        public Transform container;
        public GameObject preview;

        [Header("Items")]
        public List<GameObject> Head = new List<GameObject>();
        public List<GameObject> Torso = new List<GameObject>();
        public List<GameObject> Legs = new List<GameObject>();
        public List<GameObject> Feet = new List<GameObject>();

        public GameObject currentHead;
        public GameObject currentTorso;
        public GameObject currentLegs;
        public GameObject currentFeet;

        void Awake()
        {
            if (!instance)
            {
                DontDestroyOnLoad(gameObject);
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
        }

        void Start()
        {
            CreateLists();
            DisablePreview();
            LoadLastLoadout();
        }

        void OnLevelWasLoaded(int level)
        {
            if(GameManager.instance)
            {
                GameManager.instance.CharCustomization = this;
            }
        }

        void LoadLastLoadout()
        {
            EquipOutfit("torso", PlayerPrefs.GetString("Customizationtorso"));
            EquipOutfit("head", PlayerPrefs.GetString("Customizationhead"));
            EquipOutfit("legs", PlayerPrefs.GetString("Customizationlegs"));
            EquipOutfit("feet", PlayerPrefs.GetString("Customizationfeet"));
        }

        void CreateLists()
        {
            GameObject[] head = Resources.LoadAll<GameObject>("Prefabs/Customization/Head/");

            foreach (GameObject go in head)
            {
                Head.Add(go);
            }

            GameObject[] torso = Resources.LoadAll<GameObject>("Prefabs/Customization/Torso/");

            foreach (GameObject go in torso)
            {
                Torso.Add(go);
            }

            GameObject[] legs = Resources.LoadAll<GameObject>("Prefabs/Customization/Legs/");

            foreach (GameObject go in legs)
            {
                Legs.Add(go);
            }

            GameObject[] feet = Resources.LoadAll<GameObject>("Prefabs/Customization/Feet/");

            foreach (GameObject go in feet)
            {
                Feet.Add(go);
            }
        }

        public void EnablePreview()
        {
            if(previewCamera && !previewCamera.isActiveAndEnabled)
            {
                previewCamera.enabled = true;
                preview.SetActive(true);
            }
        }

        public void DisablePreview()
        {
            if(previewCamera &&  previewCamera.isActiveAndEnabled)
            {
                previewCamera.enabled = false;
                preview.SetActive(false);
            }
        }

        public void EnableCustomization(string type)
        {
            EnablePreview();

            title.text = type.ToUpper();

            LoadItems(type);
        }

        void ClearItems()
        {
            foreach(Transform tr in container)
            {
                Destroy(tr.gameObject);
            }
        }

        void LoadItems(string type)
        {
            ClearItems();

            GameObject non = (GameObject)Instantiate(slot, Vector3.zero, Quaternion.identity);
            non.transform.SetParent(container, false);
            non.name = "none_slot";
            non.GetComponent<Button>().onClick.AddListener(() => DeequipOutfit(type));

            switch (type)
            {
                case "head":
                    foreach(GameObject go in Head)
                    {
                        CreateSlot(go, type);
                    }
                    break;
                case "torso":
                    foreach (GameObject go in Torso)
                    {
                        CreateSlot(go, type);
                    }
                    break;
                case "legs":
                    foreach (GameObject go in Legs)
                    {
                        CreateSlot(go, type);
                    }
                    break;
                case "feet":
                    foreach (GameObject go in Feet)
                    {
                        CreateSlot(go, type);
                    }
                    break;
            }
        }

        void CreateSlot(GameObject go, string type)
        {
            GameObject it;
            it = (GameObject)Instantiate(slot, Vector3.zero, Quaternion.identity);
            it.transform.SetParent(container, false);
            string currentName = go.name;
            Button btn = it.GetComponent<Button>();
            btn.onClick.AddListener(() => EquipOutfit(type, currentName));
            it.name = currentName + "_slot";
            StartCoroutine(RenderIcon(it, go));
            
        }

        IEnumerator RenderIcon(GameObject target, GameObject go)
        {
            GameObject vis = (GameObject)Instantiate(go, Vector3.zero, Quaternion.identity);
            vis.transform.SetParent(transform, false);

            Transform iconCoord = vis.transform.FindChild("Icon");
            iconCamera.transform.localPosition = iconCoord.localPosition;
            iconCamera.transform.localRotation = iconCoord.localRotation;
            iconCamera.enabled = true;

            RenderTexture rendTex = RenderTexture.active;
            RenderTexture.active = iconCamera.targetTexture;
            iconCamera.Render();

            Texture2D img = new Texture2D(iconCamera.targetTexture.width, iconCamera.targetTexture.height);
            img.ReadPixels(new Rect(0, 0, iconCamera.targetTexture.width, iconCamera.targetTexture.height), 0, 0);
            img.Apply();
            RenderTexture.active = rendTex;

            DestroyImmediate(vis);

            iconCamera.enabled = false;

            Sprite spr = Sprite.Create(img, new Rect(0, 0, img.width, img.height), new Vector2(0.5f, 0.5f));

            target.transform.GetChild(0).GetComponent<Image>().sprite = spr;

            yield return spr;
        }

        public void EquipOutfit(string type, string slug)
        {
            switch (type)
            {
                case "head":
                    if(currentHead)
                    {
                        Destroy(currentHead);
                    }

                    foreach (GameObject go in Head)
                    {
                        if(go.name == slug)
                        {
                            GameObject it = (GameObject)Instantiate(go, Vector3.zero, Quaternion.identity);

                            Vector3 equipPos = it.GetComponent<Outfit>().equipPos;
                            Vector3 equipRot = it.GetComponent<Outfit>().equipRot;

                            it.transform.SetParent(preview.transform.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head), false);
                            it.transform.localPosition = equipPos;
                            it.transform.localRotation = Quaternion.Euler(equipRot);
                            it.name = slug;
                            currentHead = it;
                        }
                    }
                    break;
                case "torso":
                    if (currentTorso)
                    {
                        Destroy(currentTorso);
                    }

                    foreach (GameObject go in Torso)
                    {
                        if (go.name == slug)
                        {
                            GameObject it = (GameObject)Instantiate(go, Vector3.zero, Quaternion.identity);

                            it.transform.SetParent(preview.transform, false);
                            it.name = slug;
                            currentTorso = it;
                        }
                    }
                    break;
                case "legs":
                    if (currentLegs)
                    {
                        Destroy(currentLegs);
                    }

                    foreach (GameObject go in Legs)
                    {
                        if (go.name == slug)
                        {
                            GameObject it = (GameObject)Instantiate(go, Vector3.zero, Quaternion.identity);

                            it.transform.SetParent(preview.transform, false);
                            it.name = slug;
                            currentLegs = it;
                        }
                    }
                    break;
                case "feet":
                    if (currentFeet)
                    {
                        Destroy(currentFeet);
                    }

                    foreach (GameObject go in Feet)
                    {
                        if (go.name == slug)
                        {
                            GameObject it = (GameObject)Instantiate(go, Vector3.zero, Quaternion.identity);

                            it.transform.SetParent(preview.transform, false);
                            it.name = slug;
                            currentFeet = it;
                        }
                    }
                    break;
            }

            PlayerPrefs.SetString("Customization" + type, slug);
        }

        public void DeequipOutfit(string type)
        {
            switch(type)
            {
                case "head":
                    if(currentHead)
                    {
                        Destroy(currentHead);
                        currentHead = null;
                    }
                    break;
                case "torso":
                    if (currentTorso)
                    {
                        Destroy(currentTorso);
                        currentTorso = null;
                    }
                    break;
                case "legs":
                    if (currentLegs)
                    {
                        Destroy(currentLegs);
                        currentLegs = null;
                    }
                    break;
                case "feet":
                    if (currentFeet)
                    {
                        Destroy(currentFeet);
                        currentFeet = null;
                    }
                    break;
            }

            PlayerPrefs.SetString("Customization" + type, "");
        }
    }
}