﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class LocalAnimControl : MonoBehaviour
    {
        private NetworkView netView;

        //VARIABLES

        private PlayerController controller;
        private Animator anim;

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponentInParent<NetworkView>();
        }

        void Start()
        {
            if(netView.isMine)
            {
                controller = GetComponentInParent<PlayerController>();
                anim = GetComponent<Animator>();
            }
            else
            {
                enabled = false;
            }
        }

        void Update()
        {
            anim.SetFloat("Horizontal", controller.Horizontal * controller.MovementMultiplier, .2f, Time.deltaTime);
            anim.SetFloat("Vertical", controller.Vertical * controller.MovementMultiplier, .2f, Time.deltaTime);
            anim.SetBool("Swing", controller.Swinging);
            anim.SetBool("Push", controller.Pushing);
            anim.SetBool("Block", controller.Blocking);
            anim.SetInteger("WeaponType", controller.WeaponType);
            anim.SetFloat("AttackType", controller.AttackType);
            anim.SetBool("Use", controller.Interacting);

            if(controller.GotHit)
            {
                anim.SetTrigger("GetHit");
            }

            if (controller.Blocked)
            {
                anim.SetTrigger("Blocked");
            }
        }

        public void DisableSwing()
        {
            anim.SetBool("Swing", false);
        }

        public void DisablePush()
        {
            anim.SetBool("Push", false);
        }
    }
}