﻿using UnityEngine;

namespace OrangeStripes
{
    public class DevCamera : MonoBehaviour
    {
        public bool autoRotate;

        public Transform target;
        private Camera cam;

        [Range(0.1f, 20)]
        public float rotateSpeed = 10;

        public float manualRotateSpeed;

        private float posY;
        private float fov;
        private int minFov = 1;
        private int maxFov = 120;

        private float snapVal = 0.025f;

        private bool trigger;
        private bool holding;

        void Start()
        {
            cam = GetComponent<Camera>();
            fov = (int)cam.fieldOfView;
            posY = transform.localPosition.y;
        }

        void Update()
        {
            if (autoRotate)
            {
                transform.RotateAround(new Vector3(target.position.x, target.position.y + 1f, target.position.z), Vector3.up, Time.deltaTime * rotateSpeed);
            }

            if (Input.GetKey(KeyCode.Q))
            {
                posY -= snapVal;
            }
            if (Input.GetKey(KeyCode.E))
            {
                posY += snapVal;
            }

            if (Input.GetKey(KeyCode.W))
            {
                fov -= 1;
            }
            if (Input.GetKey(KeyCode.S))
            {
                fov += 1;
            }

            if (Input.GetKey(KeyCode.D))
            {
                rotateSpeed = manualRotateSpeed;
                trigger = true;
                holding = true;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                rotateSpeed = -manualRotateSpeed;
                trigger = true;
                holding = true;
            }
            else
            {
                holding = false;
            }

            if (trigger && !holding)
            {
                rotateSpeed = 10;
                trigger = false;
            }

            fov = Mathf.Clamp(fov, minFov, maxFov);

            cam.fieldOfView = fov;

            transform.localPosition = new Vector3(transform.localPosition.x, posY, transform.localPosition.z);
        }
    }
}
