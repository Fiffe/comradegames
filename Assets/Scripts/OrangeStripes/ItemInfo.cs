﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class ItemInfo : MonoBehaviour
    {
        NetworkView netView;

        //VARIABLES

        public Item item;
        public bool isProjectile;
        public int amount;

        Rigidbody rigidbody;
        Collider collider;
        ItemDatabase db;
        Animator anim;

        public Transform owner;

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
            db = GameObject.FindGameObjectWithTag("Managers").GetComponent<ItemDatabase>();
            anim = GetComponent<Animator>();
        }

        void Start()
        {
            rigidbody = GetComponent<Rigidbody>();
            collider = GetComponent<Collider>();

            if(gameObject.name != "")
            {
                item = db.FetchItemBySlug(gameObject.name);
            }

            if(transform.parent)
            {
                owner = transform.root;
            }
        }

        void OnCollisionEnter(Collision col)
        {
            if (owner == null)
            {
                if (isProjectile)
                {
                    if (col.gameObject.GetComponentInParent<PlayerMetabolism>())
                    {
                        col.gameObject.GetComponentInParent<PlayerMetabolism>().Damage(5, EDamageType.Bullet, col.gameObject.name);
                        col.gameObject.GetComponentInParent<PlayerMetabolism>().GetPushed(500, transform.position);
                    }

                    rigidbody.velocity = -rigidbody.velocity/10;
                    isProjectile = false;
                }
            }
        }

        void Update()
        {
            if (owner == null)
            {
                if(isProjectile)
                {
                    gameObject.layer = 14;
                }
                else
                {
                    gameObject.layer = 13;
                }
            }
            else
            {
                gameObject.layer = 13;
            }

            if(anim != null)
            {

            }
        }

        public void UpdateAnimator(string var, bool b)
        {
            netView.RPC("UpdateAnim", RPCMode.Others, var, b);
        }

        public void ToggleEquipped()
        {
            rigidbody.isKinematic = true;
            collider.isTrigger = true;
        }

        public void ToggleDeequipped()
        {
            rigidbody.isKinematic = false;
            collider.isTrigger = false;
        }

        [RPC]
        void UpdateAnim(string var, bool b)
        {
            anim.SetBool(var, b);
        }
    }
}