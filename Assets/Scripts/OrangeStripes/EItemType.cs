﻿using System;

namespace OrangeStripes
{
    public enum EItemType
    {
        Weapons,
        Tools,
        Wearables,
        Consumables,
        Others
    }
}