﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using LitJson;

namespace OrangeStripes
{
    public class ItemDatabase : MonoBehaviour
    {
        //VARIABLES

        private List<Item> Database = new List<Item>();
        private JsonData itemData;

        //FUNCTIONS

        void Start()
        {
            itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));
            CreateItemDatabase();
        }

        void CreateItemDatabase()
        {
            for(int i = 0; i < itemData.Count; i++)
            {
                string typeData = (string)itemData[i]["type"];
                EItemType type = (EItemType)Enum.Parse(typeof(EItemType), typeData);

                if (type == EItemType.Others)
                {
                    Database.Add(new Item((int)itemData[i]["id"], type, (string)itemData[i]["name"],
                        (string)itemData[i]["description"], (bool)itemData[i]["stackable"], (int)itemData[i]["amount"], (string)itemData[i]["slug"]));
                }
                else if(type == EItemType.Weapons)
                {
                    string weaponTypeData = (string)itemData[i]["weapon_type"];
                    string damageTypeData = (string)itemData[i]["damage_type"];
                    EWeaponType weaponType = (EWeaponType)Enum.Parse(typeof(EWeaponType), weaponTypeData);
                    EDamageType damageType = (EDamageType)Enum.Parse(typeof(EDamageType), damageTypeData);
                    Database.Add(new Weapon(weaponType, (int)itemData[i]["stats"]["damage"], (string)itemData[i]["stats"]["reach"], (string)itemData[i]["stats"]["speed"], (int)itemData[i]["id"], type, damageType, (string)itemData[i]["name"],
                        (string)itemData[i]["description"], (bool)itemData[i]["stackable"], (int)itemData[i]["amount"], (string)itemData[i]["slug"]));
                }
                else if(type == EItemType.Consumables)
                {
                    Database.Add(new Consumable((string)itemData[i]["stats"]["heal"], (string)itemData[i]["stats"]["time"], (int)itemData[i]["id"], type, (string)itemData[i]["name"],
                        (string)itemData[i]["description"], (bool)itemData[i]["stackable"], (int)itemData[i]["amount"], (string)itemData[i]["slug"]));
                }
            }
        }

        public Item FetchItemById(int id)
        {
            for (int i = 0; i < Database.Count; i++)
            {
                if (id == Database[i].Id)
                {
                    return Database[i];
                }
            }

            return null;
        }

        public Item FetchItemBySlug(string slug)
        {
            for(int i = 0; i < Database.Count; i++)
            {
                if(slug == Database[i].Slug)
                {
                    return Database[i];
                }
            }

            return null;
        }
    }
}