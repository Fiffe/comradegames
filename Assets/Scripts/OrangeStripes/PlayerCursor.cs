﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class PlayerCursor : MonoBehaviour
    {
        private NetworkView netView;
        private bool initialSetup;

        //VARIABLES

        private MultiplayerManager manager;
        private GameManager gameManager;
        private PlayerController controller;

        //PROPERTIES

        //FUNCTIONS

        void Awake()
        {
            netView = GetComponent<NetworkView>();
        }

        void InitialSetup()
        {
            if (manager)
            {
                if (!initialSetup && manager.MapLoaded)
                {
                    controller = GetComponent<PlayerController>();
                    initialSetup = true;
                }
            }
        }

        void Start()
        {
            if (netView.isMine)
            {
                if (GameManager.instance)
                {
                    gameManager = GameManager.instance;
                }

                if (gameManager)
                {
                    if (gameManager.MultiplayerManager)
                    {
                        manager = gameManager.MultiplayerManager;
                    }
                }

                InitialSetup();
            }
            else
            {
                enabled = false;
            }
        }

        void Update()
        {
            if(!gameManager)
            {
                if (GameManager.instance)
                {
                    gameManager = GameManager.instance;
                }
            }

            if(!manager)
            {
                if (gameManager)
                {
                    if (gameManager.MultiplayerManager)
                    {
                        manager = gameManager.MultiplayerManager;
                    }
                }
            }

            if(!initialSetup)
            {
                InitialSetup();
            }

            if (initialSetup)
            {
                if (manager.ShowWindow || Network.peerType == NetworkPeerType.Disconnected || !controller.IsAlive)
                {
                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.None;
                    if (controller.IsAlive)
                    {
                        controller.DisableMouse();
                        controller.CanMove = false;
                    }
                }
                else
                {
                    Cursor.visible = false;
                    Cursor.lockState = CursorLockMode.Locked;
                    if (controller.IsAlive)
                    {
                        controller.EnableMouse();
                        if (!controller.Interacting && !controller.IsStunned)
                        {
                            controller.CanMove = true;
                        }
                    }
                }
            }
        }
    }
}