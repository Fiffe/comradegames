﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace OrangeStripes
{
    public class GameManager : MonoBehaviour
    {
        //VARIABLES

        public static GameManager instance;

        private GameObject myPlayer;
        private MultiplayerManager multiplayerManager;
        private CharacterCustomization charCustomization;

        private GameObject invCanvas;
        private GameObject plrCanvas;
        private GameObject dthCanvas;
        private GameObject scrCanvas;

        //PROPERTIES

        public GameObject MyPlayer
        {
            get
            {
                return myPlayer;
            }
            set
            {
                myPlayer = value;
            }
        }

        public MultiplayerManager MultiplayerManager
        {
            get
            {
                return multiplayerManager;
            }
            set
            {
                multiplayerManager = value;
            }
        }

        public CharacterCustomization CharCustomization
        {
            get
            {
                return charCustomization;
            }
            set
            {
                charCustomization = value;
            }
        }

        //FUNCTIONS

        void Awake()
        {
            instance = this;
        }

        void Start()
        {
            SetupCanvases();
            DisableCanvases();
        }

        void SetupCanvases()
        {
            invCanvas = GameObject.Find("InventoryCanvas");
            dthCanvas = GameObject.Find("DeathCanvas");
            plrCanvas = GameObject.Find("PlayerCanvas");
            scrCanvas = GameObject.Find("ScoreCanvas");

            dthCanvas.GetComponentInChildren<Button>().onClick.AddListener(() => { Network.Disconnect(); });
        }

        public void EnableCanvases()
        {
            invCanvas.SetActive(true);
            dthCanvas.SetActive(true);
            plrCanvas.SetActive(true);
            scrCanvas.SetActive(true);
        }

        public void DisableCanvases()
        {
            invCanvas.SetActive(false);
            dthCanvas.SetActive(false);
            plrCanvas.SetActive(false);
            scrCanvas.SetActive(false);
        }
    }
}