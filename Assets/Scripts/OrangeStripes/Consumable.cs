﻿using UnityEngine;
using System.Collections;

namespace OrangeStripes
{
    public class Consumable : Item
    {
        public float Heal { get; set; }
        public float Time { get; set; }

        public Consumable(string heal, string time, int id, EItemType type, string name, string desc, bool stack, int amount, string slug)
        {
            this.Heal = float.Parse(heal);
            this.Time = float.Parse(time);
            this.Id = id;
            this.Type = type;
            this.Name = name;
            this.Description = desc;
            this.Stackable = stack;
            this.Amount = amount;
            this.Slug = slug;
            this.Sprite = Resources.Load<Sprite>("ItemIcons/" + slug);
            this.Prefab = Resources.Load<GameObject>("Prefabs/Items/" + type + "/" + Slug);
        }
    }
}