﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace OrangeStripes
{
    public class RagdollManager : MonoBehaviour
    {
        NetworkView netView;

        //VARIABLES

        public List<Collider> Cols = new List<Collider>();
        public List<Rigidbody> Rigids = new List<Rigidbody>();

        Animator[] anims;
        private bool doRagdoll = false;

        //FUNCTIONS

        void Start()
        {
            Collider[] cols;
            Rigidbody[] rigids;

            netView = GetComponent<NetworkView>();
            anims = GetComponentsInChildren<Animator>();
            rigids = GetComponentsInChildren<Rigidbody>();
            cols = GetComponentsInChildren<Collider>();

            foreach(Rigidbody rigid in rigids)
            {
                if(rigid.gameObject.layer == 10)
                {
                    rigid.isKinematic = true;
                    Rigids.Add(rigid);
                }
            }

            foreach(Collider col in cols)
            {
                if(col.gameObject.layer == 10)
                {
                    col.isTrigger = true;
                    Cols.Add(col);
                }
            }

            if(netView.isMine)
            {
                foreach(Collider col in cols)
                {
                    if (col.gameObject.layer == 10)
                    {
                        col.gameObject.layer = 11;
                    }
                }
            }
        }

        public void ResetRagdoll()
        {
            foreach (Animator anim in anims)
            {
                if (anim.gameObject.layer == 10 || anim.gameObject.layer == 11)
                {
                    anim.enabled = true;
                }
            }

            foreach (Rigidbody rigid in Rigids)
            {
                if (rigid.gameObject.layer == 10)
                {
                    rigid.isKinematic = true;
                }
            }

            foreach (Collider col in Cols)
            {
                if (col.gameObject.layer == 10 || col.gameObject.layer == 11)
                {
                    col.isTrigger = true;
                }
            }

            doRagdoll = false;
        }

        public void RagdollPlayer()
        {
            if (!doRagdoll)
            {
                foreach(Animator anim in anims)
                {
                    anim.enabled = false;
                }

                foreach (Rigidbody rig in Rigids)
                {
                    if (rig.gameObject.layer == 10 || rig.gameObject.layer == 11)
                    {
                        rig.isKinematic = false;
                    }
                }

                foreach (Collider col in Cols)
                {
                    if (col.gameObject.layer == 10 || col.gameObject.layer == 11)
                    {
                        col.isTrigger = false;
                    }
                }

                doRagdoll = true;
            }
        }
    }
}